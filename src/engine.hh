#pragma once

#include "image.hh"
#include "scene.hh"
#include "vec3.hh"

namespace rayduku {
class Engine {
public:
    Engine(Scene scene)
        : scene_(scene)
    {
    }

    Image render(unsigned height, unsigned width, unsigned nb_thread) const;

private:
    vec3 cast_ray(ray_t ray, unsigned max_iter, bool& need_antialiasing) const;

    float compute_light_at(vec3 pos, vec3 cam_ray_dir,
        Texture::texture_prop texture, vec3 normal) const;

    Scene scene_;
};
}
