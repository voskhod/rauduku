#pragma once

#include <ostream>

namespace rayduku {
class ProgressBar {
public:
    ProgressBar(float max, std::ostream& os)
        : max_(max)
        , last_(-1)
        , os_(os)
    {
        display_update(-1);
    }

    ~ProgressBar() { os_ << '\n'; }

    void display_update(float cur)
    {
        float progress = cur / max_;
        if (last_ == progress)
            return;

        last_ = progress;

        os_ << '[';
        unsigned pos = 70 * (cur / max_);

        unsigned i = 0;
        for (; i < pos; ++i)
            os_ << '=';
        if (i == pos)
            os_ << '>';
        for (; i < 70; ++i)
            os_ << ' ';

        os_ << "] " << unsigned(cur / max_ * 100.0) << " %\r";
        os_.flush();
    }

private:
    float max_;
    float last_;
    std::ostream& os_;
};
} // namespace rayduku
