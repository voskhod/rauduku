#pragma once

#include <vector>

#include "mesh.hh"
#include "perlin_noise.hh"
#include "scene.hh"

namespace rayduku {
// Generate a terrain using a perlin noise.
class Terrain {
public:
  Terrain(vec3 center, float terrain_side, unsigned mesh_size, float max_elevation);
  Terrain(vec3 center, float terrain_side, unsigned mesh_size);

  inline vec3 center() const
  {
    return center_;
  }

  // Return the elevation at (x,y). scale present the precision of the mesh.
  float get_precise_elevation(unsigned x, unsigned y, float scale) const;
  Mesh generate() const;

private:
  PerlinNoise noise_;
  std::vector<float> elevation_table_;
  std::vector<float> precise_elevation_table_;

  unsigned mesh_size_;
  float terrain_side_length_;
  float max_elevation_;
  vec3 center_;
};
}
