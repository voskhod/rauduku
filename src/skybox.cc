#include "skybox.hh"

namespace rayduku
{
color Skybox::get_color(vec3 direction) const
{
    if (!back_)
    {
      direction.normalize();
      float t = 0.5 * (direction.y() + 1.0);
      return vec3{1.0, 1.0, 1.0} * (1.0 - t) + vec3{0.5, 0.7, 1.0} * t;
    }

    unsigned nearest;
    float min;

    if (abs(direction.x()) > abs(direction.y()))
    {
        nearest = 0;
        min = abs(direction.x());
    }
    else
    {
        nearest = 1;
        min = abs(direction.y());
    }

    if (abs(direction.z()) > min)
        nearest = 2;

    auto get_color_at = [&](float x, float y, float x_offset, float y_offset)
    {
        x = x * (back_->width() - 1) / 4 + x_offset;
        y = y * (back_->height() - 1) / 3 + y_offset;
        return back_->get(y, x);
    };

    unsigned side_size = back_->height() / 3;

    if (nearest == 0) // left & right
    {
        float t = 1 / direction.x();
        direction = direction * abs(t);

        float x;
        float y = (direction.y() + 1) / 2;

        if (t > 0)
        {
            x = (-direction.z() + 1) / 2;
            return get_color_at(x, y, back_->width() / 2, side_size);
        }
        else
        {
            x = (direction.z() + 1) / 2;
            return get_color_at(x, y, 0, side_size);
        }
    }
    else if (nearest == 1) // top & bottom
    {
        float t = 1 / direction.y();
        direction = direction * abs(t);

        float x = (direction.x() + 1) / 2;

        float y;
        if (t < 0)
        {
            y = (direction.z() + 1) / 2;
            return get_color_at(x, y, (back_->width()) / 4, 0);
        }
        else
        {
            y = (direction.z() + 1) / 2;
            return get_color_at(x, y, (back_->width()) / 4, 2 * side_size);
        }
    }
    else // forward & back
    {
        float t = 1 / direction.z();
        direction = direction * t;

        if (t > 0)
        {
            float y = (direction.y() + 1) / 2;
            float x = (direction.x() + 1) / 2;
            return get_color_at(x, y, (back_->width()) / 4, (back_->height()) / 3);
        }
        else
        {
            float x = (-direction.x() + 1) / 2;
            float y = (-direction.y() + 1) / 2;
            return get_color_at(x, y, (back_->width()) / 4 * 3, (back_->height()) / 3);
        }
    }
}
}
