#pragma once

namespace rayduku {
const float epsilon = 0.001;
const float pi = 3.14;

inline float rad(float a)
{
  return a / 360 * 2 * pi;
}
} // namespace rayduku
