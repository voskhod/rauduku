#pragma once

#include <fstream>
#include <iostream>
#include <map>
#include <sstream>
#include <vector>

#include "image.hh"
#include "light.hh"
#include "skybox.hh"

namespace rayduku {

// The scene is the collection of all objects and lights. It also as the camera
// and the skybox.
class Scene {
public:
    Scene(const Camera& c, const std::string& sky_filename, const unsigned object_count=500)
        : c_(c)
        , sb_(sky_filename)
    {
        objects_.reserve(object_count);
        lights_.reserve(16);
    }

    Scene(const Camera& c, const unsigned object_count=500)
        : c_(c)
    {
        objects_.reserve(object_count);
        lights_.reserve(16);
    }

    void add_object(object_ptr o)
    {
        objects_.emplace_back(o);
    }

    void add_light(light_ptr l)
    {
        lights_.emplace_back(l);
    }

    const std::vector<object_ptr>& objects() const
    {
        return objects_;
    }

    const std::vector<light_ptr>& lights() const
    {
        return lights_;
    }

    const Camera get_camera() const
    {
        return c_;
    }

    const Skybox& sky() const
    {
        return sb_;
    }

private:
    std::vector<object_ptr> objects_;
    std::vector<light_ptr> lights_;
    const Camera c_;
    const Skybox sb_;
};
}
