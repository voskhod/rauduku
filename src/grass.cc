#include "grass.hh"

namespace rayduku
{
vec3 Bezier::find_point_on_slope(float t) const
{
    if (t > 1 || t < 0)
        throw std::runtime_error("t must be between 0 and 1");

    return p1_ * std::pow(1 - t, 3)
           + pc1_ * 3 * t * std::pow(1 - t, 2)
           + pc2_ * 3 * t * t * (1 - t)
           + p2_ * std::pow(t, 3);
}

vec3 Bezier::derivative_at_point(float t) const
{
    vec3 derivative = p1_ * -3 * std::pow(1 - t, 2)
                        + pc1_ * 3 * (std::pow(1 - t, 2) - 2 * (t - t * t))
                        + pc2_ * 3 * (2 * t - 3 * t * t)
                        + p2_ * 3 * std::pow(t, 2);

    return derivative;
}

void GrassBlade::generate_triangles(Mesh& m, int lod) const
{
    std::vector<vec3> points;
    points.reserve((lod + 1) * 2);

    float step = 1.0f / static_cast<float>(lod);
    float t = 0;

    while (t < 1)
    {
        vec3 point = bezier_curve_.find_point_on_slope(t);
        vec3 derivative = bezier_curve_.derivative_at_point(t);
        vec3 normal = derivative.product(normal_vec_);
        normal.normalize();

        vec3 point1 = point + normal * (width_ - 0.5 * width_ * t);
        vec3 point2 = point - normal * (width_ - 0.5 * width_ * t);

        points.push_back(point1);
        points.push_back(point2);
        t += step;
    }

    // Top of the blade.
    points.push_back(bezier_curve_.find_point_on_slope(1));

    for (unsigned i = 0; i < points.size() - 2; ++i)
        m.add_triangle(points[i], points[i+1], points[i+2]);
}

Mesh Grass::generate() const
{
    Mesh m;
    std::mt19937 gen(1);
    std::uniform_real_distribution real_distribution(-0.5, 0.5);

    vec3 corner; 
    if (t_ != std::nullopt)
        corner = t_->center() - vec3(1, 0 , 1) * (size_ / 2);
    else
      corner = center_ - vec3(1, 0 , 1) * (size_ / 2);

    int nb_blade_sqrt = sqrt(nb_blades_);

    float min_pos_i = 0;
    float max_pos_i = size_;
    float min_pos_j = 0;
    float max_pos_j = size_;
    float step = size_ / nb_blade_sqrt;

    for (int i = 0; i < nb_blade_sqrt; i++)
    {
        for (int j = 0; j < nb_blade_sqrt; j++)
        {
            float rand_z_offset = real_distribution(gen);
            float rand_x_offset = real_distribution(gen);
            float rand_height = real_distribution(gen);
            float rand_orientation = real_distribution(gen);
            float rand_orientation2 = real_distribution(gen);
            float rand_pos = real_distribution(gen);

            float height = grass_height_ * (1 + rand_height);
            float width = (height / 20);
            vec3 normal_vec = vec3(rand_orientation, 0, rand_orientation2);

            vec3 position;

            if (t_ == std::nullopt)
            {
                position = vec3(i * step, 0, j * step);
            }
            else
            {
                float correct_i = (float) i / (float) nb_blade_sqrt * (max_pos_i - min_pos_i) + min_pos_i;
                float correct_j = (float) j / (float) nb_blade_sqrt * (max_pos_j - min_pos_j) + min_pos_j;

                int elev_i = (float) correct_i / size_ * (float) 1000;
                int elev_j = (float) correct_j / size_ * (float) 1000;
                position = vec3(correct_i, t_->get_precise_elevation(elev_i, elev_j, 1000), correct_j);
            }

            GrassBlade blade(corner + position + vec3(rand_pos, 0, rand_pos),
                            height,
                            rand_x_offset * height,
                            rand_z_offset * height,
                            width,
                            normal_vec);

            blade.generate_triangles(m, 4);
        }
    }

    return m;
}
}
