#pragma once

#include <optional>

#include "image.hh"

namespace rayduku {
// The skybox is the background of the scene. By default the it's only a light
// blue gradient but it can be changed to any image skybox (only in ppm).
class Skybox {
public:
    Skybox()
    {
    }

    Skybox(std::string filename)
        : back_(filename)
    {
    }

    color get_color(vec3 direction) const;

private:
    std::optional<Image> back_;
};
} // namespace rayduku
