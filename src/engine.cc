#include "engine.hh"

#include <cmath>
#include <iostream>
#include <limits>
#include <thread>

#include "constants.hh"
#include "progress_bar.hh"
#include "skybox.hh"

namespace rayduku {
Image Engine::render(unsigned height, unsigned width, unsigned nb_thread) const
{
    Image img(height, width);
    Camera cam(scene_.get_camera());

    float fheight = static_cast<float>(height);
    float fwidth = static_cast<float>(width);

    ProgressBar pg(fheight, std::cout);

    auto worker = [&](unsigned id) {
        std::mt19937 gen;
        std::uniform_real_distribution<float> dis(-1, 1);

        for (float i = id; i < fheight; i += nb_thread) {
            for (float j = 0; j < fwidth; ++j) {
                bool need_antialiasing;
                ray_t r = cam.get_ray(j / fwidth - 0.5, i / fheight - 0.5);
                color c = cast_ray(r, 4, need_antialiasing);

                if (need_antialiasing) {
                    // TODO replace by custom antialiasing
                    for (unsigned n = 0; n < 7; ++n) {
                        ray_t r = cam.get_ray((j + dis(gen)) / fwidth - 0.5,
                            (i + dis(gen)) / fheight - 0.5);
                        c = c + cast_ray(r, 4, need_antialiasing);
                    }

                    c = c / 8;
                }

                img.set(i, j, c);
            }

            // Only show the progress of one thread.
            if (id == 0)
                pg.display_update(i);
        }
    };

    std::vector<std::thread> ts;
    ts.reserve(nb_thread - 1);

    for (unsigned n = 1; n < nb_thread; ++n)
        ts.emplace_back(worker, n);

    worker(0);

    for (unsigned n = 0; n < nb_thread - 1; ++n)
        ts[n].join();

    return img;
}

color Engine::cast_ray(ray_t ray, unsigned max_iter, bool& need_antialiasing) const
{
    need_antialiasing = false;
    auto [direction, origin] = ray;
    direction.normalize();

    float t_min = std::numeric_limits<float>::infinity();
    const Object* hit_object = nullptr;

    for (const auto& o : scene_.objects()) {
        float t = o->intersects(direction, origin);

        if (t > epsilon && t < t_min) {
            t_min = t;
            hit_object = o.get();
        }
    }

    if (hit_object) {
        vec3 intersection = direction * t_min + origin;
        vec3 normal = hit_object->normal(intersection, direction);
        need_antialiasing = hit_object->is_near_edge(intersection, direction);

        const auto texture = hit_object->get_texture_at(intersection);

        float l = compute_light_at(intersection, ray.first, texture, normal);

        color c = { l, l, l };

        auto mat = hit_object->get_material();
        bool na; // useless but needed to call cast_ray().

        --max_iter;
        color reflect_c;
        if (max_iter) {
            if (mat->need_antialiasing()) {
              /*
                reflect_c = { 0, 0, 0 };
                unsigned antialiasing = 128/2;
                for (unsigned n = 0; n < antialiasing; ++n) {
                    ray_t scattered
                        = mat->scatter(intersection, direction, normal);
                    reflect_c = reflect_c + cast_ray(scattered, 1, na);
                }

                reflect_c = reflect_c / antialiasing;
                */

            } else {
                ray_t scattered = mat->scatter(intersection, direction, normal);
                reflect_c = cast_ray(scattered, max_iter, na);
            }

            c = c + reflect_c;
        }

        c = c * texture.color;
        c.restrict();
        return c;
    } else
        return scene_.sky().get_color(direction);
}

float Engine::compute_light_at(vec3 pos, vec3 cam_ray_dir,
    const Texture::texture_prop texture,
    vec3 normal) const
{
    float light = 0;

    for (const auto& l : scene_.lights()) {
        vec3 direction = { l->pos() - pos };
        float len2 = direction.len2();
        float len = sqrt(len2);

        bool is_blocked = false;

        for (const auto& o : scene_.objects()) {
            float t = o->intersects(direction, pos);

            // If the object if between pos (t = 0) and l (t = len).
            if (t > epsilon && t < len) {
                is_blocked = true;
                break;
            }
        }

        if (!is_blocked) {
            direction.normalize();
            auto [color, ks] = texture;

            float tmp = 0;
            if (normal.dot(direction) > epsilon) {
                tmp = (l->intensity() * normal.dot(direction)) / len2;
                if (tmp > 0)
                    light += tmp;
            }

            cam_ray_dir.normalize();
            vec3 reflect = cam_ray_dir - normal * 2 * normal.dot(cam_ray_dir);
            tmp = ks * l->intensity() / len2
                * pow(abs(reflect.dot(direction)), 0.2);
            if (tmp > 0)
                light += tmp;
        }
    }

    return light;
}
} // namespace rayduku
