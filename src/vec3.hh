#pragma once

#include <x86intrin.h>

#include <cmath>
#include <ostream>

namespace rayduku {
class SquareMatrix4;
class vec3 {
public:
    friend SquareMatrix4;

    vec3() { data.si = float4_t { 0, 0, 0, 0 }; }
    vec3(float x, float y, float z) { data.si = float4_t { x, y, z, 0 }; }

    inline float x() const
    {
        return data.arr[0];
    }
    inline float y() const
    {
        return data.arr[1];
    }
    inline float z() const
    {
        return data.arr[2];
    }

    inline vec3 operator+(const vec3& rhs) const
    {
        return { data.si + rhs.data.si };
    }

    inline vec3 operator-(const vec3& rhs) const
    {
        return { data.si - rhs.data.si };
    }

    inline vec3 operator*(const float rhs) const
    {
        return { data.si * rhs };
    }

    inline vec3 operator*(const vec3 rhs) const
    {
        return { data.si * rhs.data.si };
    }

    inline vec3 operator/(const float rhs) const
    {
        return { data.si / rhs };
    }

    inline vec3 product(const vec3& rhs) const
    {
        mask_t m1 = { 1, 2, 0, 3 };
        mask_t m2 = { 2, 0, 1, 3 };

        return {
            __builtin_shuffle(data.si, m1) * __builtin_shuffle(rhs.data.si, m2)
            - __builtin_shuffle(data.si, m2) * __builtin_shuffle(rhs.data.si, m1)
        };
    }

    inline float dot(const vec3& rhs) const
    {
      return data.arr[0] * rhs.data.arr[0]
          + data.arr[1] * rhs.data.arr[1]
          + data.arr[2] * rhs.data.arr[2];
    }

    inline void normalize()
    {
        float l = len();
        if (__builtin_expect(l != 0, true))
            *this = *this / l;
    }

    inline void restrict()
    {
        // Replace by 1 all value greater the one.
        __m128 one = _mm_set_ps1(1);
        __m128 lt = _mm_cmplt_ps(data.si, one);
        data.si = _mm_and_ps(data.si, lt);
        one = _mm_andnot_ps(lt, one);
        data.si = _mm_or_ps(data.si, one);

        // Replace by zero all value less than zero.
        __m128 zero = _mm_set_ps1(0);
        lt = _mm_cmpge_ps(data.si, zero);
        data.si = _mm_and_ps(data.si, lt);
    }

    inline float len2() const
    {
        return dot(*this);
    }

    inline float len() const
    {
        return sqrt(len2());
    }

    inline friend std::ostream& operator<<(std::ostream& os, const vec3& rhs)
    {
        return os << rhs.x() << ' ' << rhs.y() << ' ' << rhs.z();
    }

    bool operator<(const vec3& rhs) const
    {
      return x() < rhs.x();
      if (x() != rhs.x())
        return x() < rhs.x();

      if (y() != rhs.y())
        return y() < rhs.y();

      if (z() != rhs.z())
        return z() < rhs.z();

      return false;
    }

    bool operator==(const vec3& rhs) const
    {
      return x() == rhs.x() && y() == rhs.y() && z() == rhs.z();
    }

private:
    typedef float float4_t __attribute__((vector_size(sizeof(float) * 4)));
    typedef int mask_t __attribute__((vector_size(sizeof(float) * 4)));

    vec3(float4_t v) { data.si = v; }

    // Last float is only for padding.
    union {
        float4_t si;
        float arr[4];
    } data;
};
} // namespace rayduku
