#pragma once

#include <string>
#include <vector>

#include "vec3.hh"

namespace rayduku {
typedef vec3 color;

class Image {
public:
    Image(size_t height, size_t width)
        : height_(height)
        , width_(width)
        , data_(height * width)
    {
    }

    Image(const std::string& filename);

    inline size_t height() const
    {
        return height_;
    }

    inline size_t width() const
    {
        return width_;
    }

    inline color get(size_t i, size_t j) const
    {
        return data_[i * width_ + j];
    }

    inline void set(size_t i, size_t j, const color& c)
    {
        data_[i * width_ + j] = c;
    }

    void gamma_correct();

    void save_to_ppm(const std::string& filename) const;

private:
    size_t height_;
    size_t width_;
    std::vector<color> data_;
};
} // namespace rayduku
