#pragma once

#include "vec3.hh"

namespace rayduku {
class SquareMatrix4 {
public:
    SquareMatrix4() = default;

    float operator[](unsigned i) const
    {
        return data.arr[i];
    }

    float& operator[](unsigned i)
    {
        return data.arr[i];
    }

    vec3 get_row(unsigned i) const
    {
      return data.si[i];
    }

    void set_row(vec3 val, unsigned i)
    {
      data.si[i] = val.data.si;
    }

    // vec3, under the hood, is a vec4.
    vec3 operator*(vec3 v)
    {
        v.data.arr[3] = 0;
        float4_t res = v.data.si[0] * data.si[0];
        res += v.data.si[1] * data.si[1];
        res += v.data.si[2] * data.si[2];
        res += v.data.si[3] * data.si[3];

        vec3 rv;
        rv.data.si = res;
        return rv;
    }

    // TODO: vectorize
    SquareMatrix4 operator*(const SquareMatrix4& lhs)
    {
      SquareMatrix4 res;
      for (unsigned i = 0; i < 4; ++i)
      {
        for (unsigned j = 0; j < 4; ++j)
        {
          res.data.arr[i*4+j] = 0;
          for (unsigned k = 0; k < 4; ++k)
            res.data.arr[i*4+j] += data.arr[i*4+k] * lhs.data.arr[k*4+j];
          res.data.arr[i*4+3] = 0;
        }
      }
      return res;
    }

    // Rotaion matrix on axis u (up).
    static SquareMatrix4 ru(float angle)
    {
      SquareMatrix4 m;
      m[0] = cos(angle);   m[1] = sin(angle); m[2] = 0;           m[3] = 0;
      m[4] = -sin(angle);  m[5] = cos(angle); m[6] = 0;           m[7] = 0;
      m[8] = 0;            m[9] = 0;          m[10] = 1;          m[11] = 0;
      return m;
    }

    // Rotaion matrix on axis l (left).
    static SquareMatrix4 rl(float angle)
    {
      SquareMatrix4 m;
      m[0] = cos(angle);   m[1] = 0;          m[2] = -sin(angle); m[3] = 0;
      m[4] = 0;            m[5] = 1;          m[6] = 0;           m[7] = 0;
      m[8] = sin(angle);   m[9] = 0;          m[10] = cos(angle); m[11] = 0;
      return m;
    }

    // Rotaion matrix on axis h (heading).
    static SquareMatrix4 rh(float angle)
    {
      SquareMatrix4 m;
      m[0] = 1;            m[1] = 0;          m[2] = 0;           m[3] = 0;
      m[4] = 0;            m[5] = cos(angle); m[6] = -sin(angle); m[7] = 0;
      m[8] = 0;            m[9] = sin(angle); m[10] = cos(angle); m[11] = 0;
      return m;
    }

    // Rotation on an axis u, u must be normalize.
    static SquareMatrix4 rot(float theta, vec3 u)
    {
      SquareMatrix4 m;
      float c = cos(theta);
      float s = sin(theta);

      m[0] = u.x()*u.x() * (1 - c) + c;
      m[1] = u.x()*u.y() * (1 - c) - u.z()*s;
      m[2] = u.x()*u.z() * (1 - c) + u.y()*s;
      m[3] = 0;

      m[4] = u.y()*u.x() * (1 - c) + u.z()*s;
      m[5] = u.y()*u.y() * (1 - c) + c;
      m[6] = u.y()*u.z() * (1 - c) - u.x()*s;
      m[7] = 0;

      m[8] = u.z()*u.x() * (1 - c) - u.y()*s;
      m[9] = u.z()*u.y() * (1 - c) + u.x()*s;
      m[10] = u.z()*u.z() * (1 - c) + c;
      m[11] = 0;

      return m;
    }

private:
    typedef float float4_t __attribute__((vector_size(sizeof(float) * 4)));
    union {
        float4_t si[4];
        float arr[16];
    } data;
};
}
