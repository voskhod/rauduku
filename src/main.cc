#include <cstring>
#include <iostream>
#include <string>
#include <thread>

#include "engine.hh"
#include "grass.hh"
#include "plant.hh"
#include "perlin_noise.hh"
#include "terrain.hh"
#include "matrix.hh"

using namespace rayduku;

void render_example_balls()
{
    std::string filename = "out.ppm";
    unsigned height = 200;
    unsigned width = height * 2;

    vec3 target = { 0, -6, 0 };
    vec3 cam_pos = { 0, -5, -5 };

    Camera c(cam_pos, target, {0, 1, 1}, 30, 2, 0.0,
            (cam_pos - target).len(), false);

    Scene s(c);

    s.add_object(
        Plane::make_plane(
            target + vec3 { 0, 1, 0 }, { 0, 5, 0 },
            UniformTexture::make_uniform_texture({ 0.0, 0.7, 0.0 }, 0),
            Matt::make_matt())
      );

    s.add_object(
        Sphere::make_sphere(
            target + vec3 { 3, 0, 15 }, 10,
            UniformTexture::make_uniform_texture({ 0.75, 0.75, 0.75 }, 0.0),
            Metal::make_metal()));

    s.add_object(
        Sphere::make_sphere(
            target + vec3 { 3, 0, 0 }, 2,
            UniformTexture::make_uniform_texture({ 0.0, 0.5, 0.7 }, 0.2),
            Matt::make_matt()));

    s.add_object(
        Sphere::make_sphere(
            target + vec3 { -4, -1, 0 }, 1.5,
            UniformTexture::make_uniform_texture({ .83, 0.68, 0.21 }, 0.0),
            Metal::make_metal()));

    s.add_light(PointLight::make_point_light({ 7, -7, -7 }, 100));

    Engine e(s);
    unsigned nb_thread = std::thread::hardware_concurrency();

    std::cout << "Rendering " << s.objects().size() << " objects...\n";
    std::cout << "Launching " << nb_thread << " threads...\n";
    Image img = e.render(height, width, nb_thread);

    std::cout << "Finished !\nSaving the image in '" << filename << "'...\n";
    img.save_to_ppm(filename);

    std::cout << "Done !\n";
}

void render_example_bush()
{
    std::string filename = "out.ppm";
    unsigned height = 200;
    unsigned width = height * 2;

    vec3 target = { 0, 0, 0 };
    vec3 cam_pos = { 0, 0, -5 };

    Camera c(cam_pos, target, {0, 1, 1}, 30, 2, 0.0,
            (cam_pos - target).len(), false);

    Scene s(c);

    Mesh m = generate_bush();
    m.add_triangles_to_scene(s, {vec3(1,0,0), vec3(0,0,1)});

    s.add_light(PointLight::make_point_light({ 7, -7, -7 }, 100));

    Engine e(s);
    unsigned nb_thread = std::thread::hardware_concurrency();

    std::cout << "Rendering " << s.objects().size() << " objects...\n";
    std::cout << "Launching " << nb_thread << " threads...\n";
    Image img = e.render(height, width, nb_thread);

    std::cout << "Finished !\nSaving the image in '" << filename << "'...\n";
    img.save_to_ppm(filename);

    std::cout << "Done !\n";
}

void mesh_example()
{
    std::string filename = "out.obj";
    Terrain terrain(vec3(2, 0.6, -2.3), 20, 50);
    Mesh t = terrain.generate();

    Grass grass(vec3(0, 5, -1), 20, 100000, 0.2, terrain);
    Mesh g = grass.generate();

    Mesh b = generate_bush();

    t.merge(std::move(g));
    t.merge(std::move(b));

    std::cout << "Saving mesh..." << std::endl;
    t.save(filename);
    std::cout << "Done !\n";
}

int main(int argc, char* argv[])
{
    auto help = [] { std::cerr << "Usage: rayduku (-render1|-render2|-mesh)\n"; };

    if (argc != 2)
    {
      help();
      return 1;
    }

    if (strcmp(argv[1], "-render1") == 0)
      render_example_balls();
    else if (strcmp(argv[1], "-render2") == 0)
      render_example_bush();
    else if (strcmp(argv[1], "-mesh") == 0)
      mesh_example();
    else
    {
      help();
      return 1;
    }

    return 0;
}
