#pragma once

#include "mesh.hh"
#include "terrain.hh"
#include "vec3.hh"

namespace rayduku {
// Bezier cuves generator, can conpute points on the curve and the tangents.
class Bezier
{
public:
    inline Bezier(const vec3& p1, const vec3& p2, const vec3& pc1, const vec3& pc2)
        : p1_(p1)
        , p2_(p2)
        , pc1_(pc1)
        , pc2_(pc2)
    {
    }

    Bezier(const vec3& p1, const vec3& p2)
        : p1_(p1)
        , p2_(p2)
        , pc1_(p1 + vec3(0, (p2.y() - p1.y()) / 3, 0))
        , pc2_(p1 + vec3(0, (p2.y() - p1.y()) * 2 / 3, 0))
    {
    }

    vec3 find_point_on_slope(float t) const;
    vec3 derivative_at_point(float t) const;

private:
    const vec3 p1_;
    const vec3 p2_;
    const vec3 pc1_;
    const vec3 pc2_;
};

// A grass blade. The curvature of the blade is a Bezeier curve.
class GrassBlade {
public:
    GrassBlade(const vec3& basePoint, float height)
        : base_point_(basePoint)
        , height_(height)
        , z_offset_(0.5)
        , x_offset_(0)
        , width_(height / 16)
        , normal_vec_(vec3(1, 0, 4))
        , bezier_curve_(basePoint, basePoint + vec3(x_offset_, -height, z_offset_))
    {
    }

    GrassBlade(const vec3& basePoint, float height, float z_offset, float x_offset,
        float width, const vec3& normalVec)
        : base_point_(basePoint)
        , height_(height)
        , z_offset_(z_offset)
        , x_offset_(x_offset)
        , width_(width)
        , normal_vec_(normalVec)
        , bezier_curve_(basePoint, basePoint + vec3(x_offset, -height, z_offset))
    {
    }

    void generate_triangles(Mesh& s, int lod) const;

private:
    const vec3 base_point_;      // The base point of the blade
    const float height_;         // The height (y axis) of the blade
    const float z_offset_;       // The offset (z axis) of the tip of the blade
    const float x_offset_;       // The offset (x axis) of the tip of the blade
    const float width_;          // The width at the base of the blade
    const vec3 normal_vec_;      // A vector normal to the blade's surface

    const Bezier bezier_curve_;  // The bezier curve representing the coordinates
                                 // of the middle of the blade
};

// Generate grass.
class Grass {
public:
    // If terrain is given, grass will be generate at the surface of the
    // terrain.
    Grass(const vec3& center, float size, int nb_blades, float height, Terrain& t)
        : center_(center)
        , size_(size)
        , nb_blades_(nb_blades)
        , grass_height_(height)
        , t_(t)
    {
    }

    Grass(const vec3& center, float size, int nb_blades, float height)
        : center_(center)
        , size_(size)
        , nb_blades_(nb_blades)
        , grass_height_(height)
        , t_(std::nullopt)
    {
    }

    Mesh generate() const;

private:
    const vec3 center_;
    const float size_;
    const int nb_blades_;
    const float grass_height_;
    const std::optional<Terrain> t_;
};
}
