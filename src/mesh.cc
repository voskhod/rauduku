#include "mesh.hh"

namespace rayduku
{
  void Mesh::add_triangle(vec3 a, vec3 b, vec3 c, unsigned color)
  {
    int base_id = vertices_.size();
    vertices_.push_back(a);
    vertices_.push_back(b);
    vertices_.push_back(c);

    max_color_ = std::max(color, max_color_);

    triangles_.emplace_back(base_id, base_id + 1, base_id + 2, color);
  }

  void Mesh::add_triangles_to_scene(Scene& s, const std::vector<vec3> cm) const
  {
    if (cm.size() != (max_color_ + 1))
      throw std::invalid_argument("Mesh::add_triangles_to_scene():"
          "invalid color map, expected "
          + std::to_string(max_color_ + 1) + " colors, get "
          + std::to_string(cm.size()));

    for (const auto& t : triangles_)
    {
      auto texture = UniformTexture::make_uniform_texture(cm[t.color], 0);
      auto material = Matt::make_matt();
      
      s.add_object(Triangle::make_triangle(
            vertices_[t.vert1],
            vertices_[t.vert2],
            vertices_[t.vert3],
            texture, material));
    }
  }

  void Mesh::save(const std::string& filename) const
  {
    std::fstream f(filename, std::fstream::out);
    f << to_str();
  }

  std::string Mesh::to_str() const
  {
    std::stringstream ss;

    for (const Mesh* cur = this; cur; cur = cur->next_.get())
    {
      for (auto vert : cur->vertices_)
      {
        ss << "v " << -vert.x()
          << " " << -vert.y()
          << " " << -vert.z() << "\n";
      }
    }

    for (const Mesh* cur = this; cur; cur = cur->next_.get())
    {
      for (const auto& triangle : cur->triangles_)
      {
        ss << "f " << triangle.vert1 + 1
          << " " << triangle.vert2 + 1
          << " " << triangle.vert3 + 1
          << "\n";
      }
    }

    return ss.str();
  }

  void Mesh::merge(Mesh&& other)
  {
    max_color_ = std::max(max_color_, other.max_color_);

    other.next_ = std::move(next_);
    next_ = std::make_unique<Mesh>(std::move(other));
  }

  void Mesh::resize(float factor)
  {
    for (Mesh* cur = this; cur; cur = cur->next_.get())
      for (vec3& v : cur->vertices_)
        v = v * factor;
  }

  void Mesh::translate(vec3 offset)
  {
    for (Mesh* cur = this; cur; cur = cur->next_.get())
      for (vec3& v : cur->vertices_)
        v = v + offset;
  }

  void Mesh::rotate(SquareMatrix4 rot)
  {
    for (Mesh* cur = this; cur; cur = cur->next_.get())
      for (vec3& v : cur->vertices_)
        v = rot * v;
  }
}
