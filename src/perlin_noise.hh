#pragma once

#include <algorithm>
#include <cmath>
#include <cstdint>
#include <random>
#include <vector>

// Perlin noise is a procedural pseudo-random texture. This a C++ translation
// of https://mrl.cs.nyu.edu/~perlin/noise/.
class PerlinNoise {
public:
    PerlinNoise(unsigned seed=1)
    {
        for (size_t i = 0; i < 256; ++i)
            p[i] = static_cast<std::uint8_t>(i);

        std::shuffle(std::begin(p), std::begin(p) + 256, std::default_random_engine(seed));

        for (size_t i = 0; i < 256; ++i)
            p[256 + i] = p[i];
    }

    // Return the value at (x,y,z).
    double get(double x, double y, double z) const
    {
        // Find unit cube that conatains point.
        int X = (int)std::floor(x) & 255;
        int Y = (int)std::floor(y) & 255;
        int Z = (int)std::floor(z) & 255;

        // Get coordianate relatite to (x, y, z) point in cuvbe.
        x -= std::floor(x);
        y -= std::floor(y);
        z -= std::floor(z);

        // Compute fade curves.
        double u = fade(x);
        double v = fade(y);
        double w = fade(z);

        // Hash coordinates of the cube corners.
        int A = p[X] + Y;
        int AA = p[A] + Z;
        int AB = p[A + 1] + Z;
        int B = p[X + 1] + Y;
        int BA = p[B] + Z;
        int BB = p[B + 1] + Z;

        // Add blended resulsts from 8 corners.
        return lerp(w,
                    lerp(v,
                         lerp(u,
                              grad(p[AA], x, y, z),
                              grad(p[BA], x - 1, y, z)),
                         lerp(u,
                              grad(p[AB], x, y - 1, z),
                              grad(p[BB], x - 1, y - 1,
                         z))),
                    lerp(v,
                         lerp(u,
                              grad(p[AA + 1], x, y, z - 1),
                              grad(p[BA + 1], x - 1, y, z - 1)),
                         lerp(u,
                              grad(p[AB + 1], x, y - 1, z - 1),
                              grad(p[BB + 1], x - 1, y - 1, z - 1))));
    }

    // Smooth function smoothstep.
    static inline double fade(double t)
    {
        return t * t * t * (t * (t * 6 - 15) + 10);
    }

    // Linear interpolation of curbe describe by (a,b) at t.
    static inline double lerp(double t, double a, double b)
    {
        return a + t * (b - a);
    }

    static inline double grad(int hash, double x, double y, double z)
    {
        // This function compute the gradient using a hash method.  Only the 4
        // last bits are usefull as there is only 12 gradient directions
        // possbles.
        int h = hash & 15;

        double u = h < 8 ? x : y;
        double v = h < 4 ? y : h == 12 || h == 14 ? x : z;

        return ((h & 1) == 0 ? u : -u) + ((h & 2) == 0 ? v : -v);
    }
private:
    std::array<uint8_t,  512> p;
};
