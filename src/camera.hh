#pragma once

#include <cmath>
#include <random>

#include "constants.hh"
#include "vec3.hh"

namespace rayduku {
typedef std::pair<vec3, vec3> ray_t;
// The camera will generate the ray for each image pixels.
class Camera {
public:
    Camera(vec3 pos, vec3 target, vec3 up, float fov, float aspect,
        float aperture, float focus_dist, bool random_ray)
        : pos_(pos)
        , random_ray_(random_ray)
    {
        float theta = fov * pi / 180;
        float half_height = tan(theta / 2) * focus_dist;
        float half_width = half_height * aspect;

        w_ = (pos_ - target);
        w_.normalize();
        u_ = up.product(w_);
        u_.normalize();
        v_ = w_.product(u_);

        horizontal_ = u_ * half_width * 2 * focus_dist;
        vertical_ = v_ * half_height * 2 * focus_dist;

        lens_radius_ = aperture / 2;
    }

    ray_t get_ray(float i, float j) const
    {
        // TODO: add random for focus dist
        vec3 direction = horizontal_ * i + vertical_ * j - pos_;
        return { direction, pos_ };
    }

    static vec3 random_in_disk()
    {
        static thread_local std::mt19937 gen;
        std::uniform_real_distribution<float> dis(-1, 1);
        vec3 v;

        do {
            v = { dis(gen), dis(gen), 0 };
        } while (v.len2() >= 1);

        return v;
    }

    static vec3 random_in_sphere()
    {
        static thread_local std::mt19937 gen;
        std::uniform_real_distribution<float> dis(-1, 1);
        vec3 v;

        do {
            v = { dis(gen), dis(gen), dis(gen) };
        } while (v.len2() >= 1);

        return v;
    }

private:
    vec3 pos_;
    vec3 horizontal_;
    vec3 vertical_;
    vec3 lower_left_;

    // The units vectors
    vec3 u_;
    vec3 v_;
    vec3 w_;

    float lens_radius_;
    bool random_ray_;
};
} // namespace rayduku
