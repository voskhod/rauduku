#pragma once

#include <memory>

#include "camera.hh"
#include "image.hh"
#include "matrix.hh"
#include "vec3.hh"

namespace rayduku {
class Material {
public:
    virtual ray_t scatter(vec3 intersection, vec3 direction, vec3 normal) const = 0;
    virtual bool need_antialiasing() const = 0;
};

typedef std::shared_ptr<Material> material_ptr;

class Metal : public Material {
public:
    ray_t scatter(vec3 intersection, vec3 direction, vec3 normal) const
    {
        vec3 reflect_dir = direction - normal * 2 * normal.dot(direction);
        reflect_dir = reflect_dir + Camera::random_in_sphere() * fuzziness;
        return { reflect_dir, intersection };
    }

    inline bool need_antialiasing() const
    {
        return false;
    }

    static material_ptr make_metal()
    {
        return std::make_shared<Metal>();
    }

private:
    float fuzziness = 0;
};

class Matt : public Material {
public:
    ray_t scatter(vec3 intersection, vec3 direction, vec3 normal) const
    {
        (void)direction;
        vec3 dir_reflect = Camera::random_in_sphere();

        dir_reflect.normalize();
        if (dir_reflect.dot(normal) < 0.0)
            dir_reflect = dir_reflect * -1;
        return { dir_reflect, intersection };
    }

    inline bool need_antialiasing() const
    {
        return true;
    }

    static material_ptr make_matt()
    {
        return std::make_shared<Matt>();
    }
};

class Texture {
public:
    struct texture_prop {
        // K_d * c, diffusion * couleur
        vec3 color;
        float ks;
    };

    virtual texture_prop get_texture_at(vec3 pos) const = 0;
};

typedef std::shared_ptr<Texture> texture_ptr;

class UniformTexture : public Texture {
public:
    UniformTexture(vec3 color, float ks)
        : color_(color)
        , ks_(ks)
    {
    }

    virtual ~UniformTexture() { }

    static texture_ptr make_uniform_texture(vec3 color, float ks)
    {
        return std::make_shared<UniformTexture>(color, ks);
    }

    texture_prop get_texture_at(vec3) const { return { color_, ks_ }; }

private:
    vec3 color_;
    float ks_;
    float metal_;
};

class ImageTexture : public Texture {
public:
    ImageTexture(const std::string& path, vec3 up, vec3 left)
        : img_(path)
        , up_(up)
        , left_(left)
    {
    }

    virtual ~ImageTexture() { }

    static texture_ptr make_image_texture(const std::string& path, vec3 up, vec3 left)
    {
        return std::make_shared<ImageTexture>(path, up, left);
    }

    texture_prop get_texture_at(vec3 pos) const
    {
        float x = pos.dot(left_);
        float y = pos.dot(up_);
        float r = 3.14 / abs(y);

        x = (x + 1) / 2 * static_cast<float>(img_.width() - 1) * r;
        y = (y + 1) / 2 * static_cast<float>(img_.height() - 1);

        if (x >= img_.width())
            x = img_.width() - 1;

        if (y >= img_.width())
            y = img_.height() - 1;

        return { img_.get(static_cast<unsigned>(y), static_cast<unsigned>(x)),
            0 };
    }

private:
    Image img_;
    vec3 up_;
    vec3 left_;
};

class Object {
public:
    Object(texture_ptr texture, material_ptr material)
        : texture_(texture)
        , material_(material)
    {
    }

    // 0 => no intersection else the t parameter where the ray intersects.
    virtual float intersects(vec3 direction, vec3 origin) const = 0;
    virtual vec3 normal(vec3 p, vec3 direction) const = 0;
    virtual Texture::texture_prop get_texture_at(vec3 pos) const = 0;
    virtual bool is_near_edge(vec3 pos, vec3 direction) const = 0;

    material_ptr get_material() const
    {
        return material_;
    }

protected:
    const texture_ptr texture_;
    const material_ptr material_;
};

typedef std::shared_ptr<Object> object_ptr;

class Plane : public Object {
public:
    Plane(const vec3& normal, const vec3& point, const texture_ptr& texture,
        const material_ptr& material)
        : Object(texture, material)
        , normal_(normal)
        , point_(point)
    {
    }

    static std::shared_ptr<Object>
    make_plane(const vec3& normal, const vec3& point,
                const texture_ptr& texture, const material_ptr& material)
    {
        return std::make_shared<Plane>(normal, point, texture, material);
    }

    float intersects(const vec3 direction, const vec3 origin) const override;

    vec3 normal(vec3 p, vec3 direction) const override
    {
        (void)p;
        if (direction.dot(normal_) > 0)
            return normal_ * -1;
        return normal_;
    }

    inline Texture::texture_prop get_texture_at(const vec3 pos) const override
    {
        return texture_->get_texture_at(pos);
    }

    inline bool is_near_edge(const vec3 p, const vec3 dir) const override
    {
        (void)p;
        return std::abs(dir.dot(normal_)) < 0.1;
    }

private:
    vec3 normal_;
    vec3 point_;
};

class Sphere : public Object {
public:
    Sphere(vec3 pos, float radius, texture_ptr texture, material_ptr material)
        : Object(texture, material)
        , pos_(pos)
        , radius_(radius)
    {
    }

    inline static object_ptr
    make_sphere(vec3 pos, float radius, texture_ptr texture, material_ptr material)
    {
        return std::make_shared<Sphere>(pos, radius, texture, material);
    }

    float intersects(vec3 direction, vec3 origin) const;

    vec3 normal(vec3 p, vec3 direction) const
    {
        (void)direction;
        vec3 n = (p - pos_) / radius_;
        return n;
    }

    inline Texture::texture_prop get_texture_at(vec3 pos) const
    {
        pos = pos - pos_;
        pos.normalize();
        return texture_->get_texture_at(pos);
    }

    inline bool is_near_edge(vec3 p, vec3 dir) const
    {
        return std::abs(normal(p, dir).dot(dir)) < 0.1;
    }

private:
    vec3 pos_;
    float radius_;
};

// Faster triangle as describe in Fast Ray-Triangle Intersections by CoordinateTransformation by
// Doug Baldwin & Michael Weber
class PreComputedTriangle : public Object {
public:
    PreComputedTriangle(vec3 a, vec3 b, vec3 c, texture_ptr texture, material_ptr material);

    inline static object_ptr
    make_precomputedtriangle(vec3 a, vec3 b, vec3 c, texture_ptr texture, material_ptr material)
    {
        return std::make_shared<PreComputedTriangle>(a, b, c, texture, material);
    }

    float intersects(vec3 direction, vec3 origin) const;

    vec3 normal(vec3 p, vec3 direction) const
    {
        (void)p;
        if (normal_.dot(direction) > 0)
            return normal_ * -1;
        else
            return normal_;
    }

    Texture::texture_prop get_texture_at(vec3 pos) const
    {
        return texture_->get_texture_at(pos);
    }

    bool is_near_edge(vec3 p, vec3 dir) const
    {
        (void)p;
        (void)dir;
        return true; // TODO
    }

private:
    vec3 normal_;
    SquareMatrix4 transform_;
};

class Triangle : public Object {
public:
    Triangle(vec3 a, vec3 b, vec3 c, texture_ptr texture, material_ptr material)
        : Object(texture, material)
        , a_(a)
        , b_(b)
        , c_(c)
    {
        normal_ = (a - b).product(a - c);
        normal_.normalize();
    }

    inline static object_ptr
    make_triangle(vec3 a, vec3 b, vec3 c, texture_ptr texture, material_ptr material)
    {
        return std::make_shared<Triangle>(a, b, c, texture, material);
    }

    float intersects(vec3 direction, vec3 origin) const;

    vec3 normal(vec3 p, vec3 direction) const
    {
        (void)p;
        if (normal_.dot(direction) > 0)
            return normal_ * -1;
        else
            return normal_;
    }

    Texture::texture_prop get_texture_at(vec3 pos) const
    {
        return texture_->get_texture_at(pos);
    }

    bool is_near_edge(vec3 p, vec3 dir) const
    {
        (void)p;
        (void)dir;
        return true; // TODO
    }

private:
    vec3 a_;
    vec3 b_;
    vec3 c_;
    vec3 normal_;
};

class Cylinder : public Object {
public:
    Cylinder(vec3 pos, vec3 up, float len, float radius,
        texture_ptr texture, material_ptr material)
        : Object(texture, material)
        , pos_(pos)
        , up_(up)
        , len_(len)
        , radius_(radius)
    {
        up_.normalize();
    }

    inline static object_ptr
    make_cylinder(vec3 pos, vec3 up, float len, float radius,
        texture_ptr texture, material_ptr material)
    {
        return std::make_shared<Cylinder>(pos, up, len, radius, texture, material);
    }

    float intersects(vec3 direction, vec3 origin) const;

    vec3 normal(vec3 p, vec3 direction) const
    {
        (void)direction;
        return { p.x() - pos_.x(), p.y(), p.z() - pos_.z() };
    }

    Texture::texture_prop get_texture_at(vec3 pos) const
    {
        return texture_->get_texture_at(pos);
    }

    bool is_near_edge(vec3 p, vec3 dir) const
    {
        (void)p;
        (void)dir;
        return true; // TODO
    }

private:
    vec3 pos_;
    vec3 up_;
    float len_;
    float radius_;
};
} // namespace rayduku
