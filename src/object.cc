#include "object.hh"

#include <cmath>
#include <iostream>

namespace rayduku {
float Sphere::intersects(vec3 direction, vec3 origin) const
{
    // changes reference, the center of the sphere is the origin
    origin = origin - pos_;

    float a = direction.len2();
    float half_b = origin.dot(direction);
    float c = origin.len2() - radius_ * radius_;

    float delta = half_b * half_b - a * c;

    // FIXME
    if (delta > 0.0)
        return (-half_b - sqrt(delta)) / a;
    else
        return 0;
}

float Plane::intersects(const vec3 direction, const vec3 origin) const
{
    float dot_normal_direction = normal_.dot(direction);
    if (std::abs(dot_normal_direction) < epsilon)
        return 0;
    else
        return ((point_ - origin).dot(normal_)) / (dot_normal_direction);
}

PreComputedTriangle::PreComputedTriangle(vec3 a, vec3 b, vec3 c, texture_ptr texture, material_ptr material)
    : Object(texture, material)
{
    const vec3 edge1 = b - a;
    const vec3 edge2 = c - a;

    normal_ = edge1.product(edge2);

    float x1, x2;
    const float n = a.dot(normal_);

    if (std::abs(normal_.x()) > std::abs(normal_.y())
        && std::abs(normal_.x()) > std::abs(normal_.z())) {
        x1 = b.y() * a.z() - b.z() * a.y();
        x2 = c.y() * a.z() - c.z() * a.y();

        transform_[0] = 0;
        transform_[1] = edge2.z() / normal_.x();
        transform_[2] = -edge2.y() / normal_.x();
        transform_[3] = x2 / normal_.x();

        transform_[4] = 0;
        transform_[5] = -edge1.z() / normal_.x();
        transform_[6] = edge1.y() / normal_.x();
        transform_[7] = -x1 / normal_.x();

        transform_[8] = 1;
        transform_[9] = normal_.y() / normal_.x();
        transform_[10] = normal_.z() / normal_.x();
        transform_[11] = -n / normal_.x();
    } else if (std::abs(normal_.y()) > std::abs(normal_.z())) {
        x1 = b.z() * a.x() - b.x() * a.z();
        x2 = c.z() * a.x() - c.x() * a.z();

        transform_[0] = -edge2.z() / normal_.y();
        transform_[1] = 0;
        transform_[2] = edge2.x() / normal_.y();
        transform_[3] = x2 / normal_.y();

        transform_[4] = edge1.z() / normal_.y();
        transform_[5] = 0;
        transform_[6] = -edge1.x() / normal_.y();
        transform_[7] = -x1 / normal_.y();

        transform_[8] = normal_.x() / normal_.y();
        transform_[9] = 1;
        transform_[10] = normal_.z() / normal_.y();
        transform_[11] = -n / normal_.y();
    } else if (std::abs(normal_.z()) > 0) {
        x1 = b.x() * a.y() - b.y() * a.x();
        x2 = c.x() * a.y() - c.y() * a.x();

        transform_[0] = edge2.y() / normal_.z();
        transform_[1] = -edge2.x() / normal_.z();
        transform_[2] = 0;
        transform_[3] = x2 / normal_.z();

        transform_[4] = -edge1.y() / normal_.z();
        transform_[5] = edge1.x() / normal_.z();
        transform_[6] = 0;
        transform_[7] = -x1 / normal_.z();

        transform_[8] = normal_.x() / normal_.z();
        transform_[9] = normal_.y() / normal_.z();
        transform_[10] = 1;
        transform_[11] = -n / normal_.z();
    } else {
        throw std::runtime_error("PreComputedTriangle(): degenerate triangle");
    }

    // Need to be here for some reason
    normal_.normalize();
}

float PreComputedTriangle::intersects(vec3 direction, vec3 origin) const
{
    const float projected_dir = transform_[8] * direction.x()
        + transform_[9] * direction.y()
        + transform_[10] * direction.z();

    if (!projected_dir)
        return 0;

    const float projected_ori = transform_[8] * origin.x()
        + transform_[9] * origin.y()
        + transform_[10] * origin.z()
        + transform_[11];

    const float ta = -projected_ori / projected_dir;

    if (ta <= epsilon || ta >= 10000)
        return 0;

    const vec3 wr = origin + direction * ta;

    // Compute coordinate relatives to the barycentre
    const float xg = transform_[0] * wr.x()
        + transform_[1] * wr.y()
        + transform_[2] * wr.z()
        + transform_[3];

    const float yg = transform_[4] * wr.x()
        + transform_[5] * wr.y()
        + transform_[6] * wr.z()
        + transform_[7];

    if (xg >= 0.0f && yg >= 0.0f && yg + xg < 1.0f)
        return ta;

    return 0;
}

float Triangle::intersects(vec3 direction, vec3 origin) const
{
    //TODO Fast Ray-Triangle Intersections by CoordinateTransformation
    // BALDWIN
    vec3 ab = b_ - a_;
    vec3 ac = c_ - a_;
    vec3 h = direction.product(ac);
    float alpha = ab.dot(h);
    if (alpha > -epsilon && alpha < epsilon)
        return 0;

    float f = 1.0 / alpha;
    vec3 s = origin - a_;
    float beta = f * s.dot(h);
    if (beta < 0 || beta > 1)
        return 0;

    vec3 q = s.product(ab);
    float gamma = f * direction.dot(q);
    if (gamma < 0 || gamma + beta > 1)
        return 0;

    return f * ac.dot(q);
}

float Cylinder::intersects(vec3 direction, vec3 origin) const
{
    //Check the endcap
    float t_top = (pos_.y() + len_ - origin.y()) / direction.y();
    float d = (pos_ + vec3 { 0, len_, 0 } - (direction * t_top + origin)).len();
    if (d < radius_)
        return t_top;

    float t_bot = (pos_.y() - origin.y()) / direction.y();
    d = (pos_ - (direction * t_top + origin)).len();
    if (d < radius_)
        return t_bot;

    // Check body of cylinder
    vec3 c_direction = direction * vec3 { 1, 0, 1 };

    float a = direction.x() * direction.x() + direction.z() * direction.z();
    float half_b = c_direction.dot(origin - pos_);
    float c = (origin.x() - pos_.x()) * (origin.x() - pos_.x())
        + (origin.z() - pos_.z()) * (origin.z() - pos_.z()) - (radius_ * radius_);

    float delta = half_b * half_b - (a * c);

    // negative and zero
    if (delta < epsilon)
        return 0;

    //TODO
    /*
    float t;
    if (delta == 0)
      t = -half_b / a;
    else
      t = (-half_b - sqrt(delta)) / (a);
      */
    float t1 = (-half_b - sqrt(delta)) / a;
    float t2 = (-half_b + sqrt(delta)) / a;
    float t;

    if (t1 > t2)
        t = t2;
    else
        t = t1;

    float h = direction.y() * t + origin.y() - pos_.y();
    if (h < 0 || h > len_)
        return 0;

    return t;
}
} // namespace rayduku
