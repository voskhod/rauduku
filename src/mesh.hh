#pragma once

#include <vector>
#include "scene.hh"
#include "vec3.hh"
#include "matrix.hh"

namespace rayduku
{
class Mesh
{
public:
  // n_triangles is not a hard limit, it's only an hint to avoid to many
  // reallocation.
  Mesh(unsigned n_triangles=1000)
  {
    vertices_.reserve(n_triangles * 3);
    triangles_.reserve(n_triangles);
  }

  Mesh(Mesh&& other)
    : max_color_(other.max_color_)
    , vertices_(std::move(other.vertices_))
    , triangles_(std::move(other.triangles_))
    , next_(std::move(other.next_))
  {
  }

  void add_triangle(vec3 a, vec3 b, vec3 c, unsigned color=0);
  void merge(Mesh&& other);
  void resize(float factor);
  void translate(vec3 offset);
  void rotate(SquareMatrix4 rot);

  // Add a mesh to a scene according to a color map.
  void add_triangles_to_scene(Scene& s, const std::vector<vec3> cm) const;
  void save(const std::string& filename) const;
  std::string to_str() const;

private:
  struct MeshTriangle
  {
    MeshTriangle(unsigned v1, unsigned v2, unsigned v3, unsigned c)
      : vert1(v1)
      , vert2(v2)
      , vert3(v3)
      , color(c)
    {}

    unsigned vert1, vert2, vert3, color;
  };

  unsigned max_color_ = 0;
  std::vector<vec3> vertices_;
  std::vector<MeshTriangle> triangles_;

  // Usefull for merging mesh.
  std::unique_ptr<Mesh> next_ = nullptr;
};
}
