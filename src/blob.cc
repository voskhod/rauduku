#include "blob.hh"
#include "blob_table.hh"

namespace rayduku {

Blob::Blob(vec3 center, float side_size, int step_count)
    : corner_(vec3(0, 0, 0))
{
    step_count_ = step_count;
    step_ = side_size / static_cast<float>(step_count_);
    corner_ = center - vec3((side_size / 2), (side_size / 2), (side_size / 2));
    potential_points.reserve(200);
}

float Blob::find_potential(int x, int y)
{
    float pt_potential = 0;
    vec3 point_evaluated = corner_ + vec3(0.25, y * step_, x * step_);
    for (auto point : potential_points) {
        float norm = (point.pos - point_evaluated).len2();
        pt_potential += 1 / (norm)*point.val;
    }
    return std::abs(pt_potential);
}

float Blob::find_potential(int x, int y, int z)
{
    float pt_potential = 0;
    vec3 point_evaluated = corner_ + vec3(x, y, z) * step_;

    for (auto point : potential_points) {
        float norm = (point.pos - point_evaluated).len2();

        if (__builtin_expect(norm != 0, true))
            pt_potential += 1 / (norm)*point.val;
        else {
            // evaluated point is at potential point
            pt_potential = std::numeric_limits<float>::infinity();
            break;
        }
    }
    return pt_potential;
}

Mesh Blob::generate(float potential)
{
    Mesh m;
    unsigned n = step_count_;
    std::vector<bool> is_iso(n * n * n);

    for (int x = 0; x < step_count_; x++) {
        for (int y = 0; y < step_count_; y++) {
            for (int z = 0; z < step_count_; z++) {
                is_iso[x * n * n + y * n + z] = (find_potential(x, y, z) > potential);
            }
        }
    }

    const auto get_val = [&](unsigned x, unsigned y, unsigned z) -> bool {
        return is_iso[x * n * n + y * n + z];
    };

    for (int x = 0; x < step_count_ - 1; x++) {
        for (int y = 0; y < step_count_ - 1; y++) {
            for (int z = 0; z < step_count_ - 1; z++) {

                int index = 0;
                if (get_val(x + 1, y + 1, z))     index |= 1;
                if (get_val(x + 1, y + 1, z + 1)) index |= 2;
                if (get_val(x, y + 1, z + 1))     index |= 4;
                if (get_val(x, y + 1, z))         index |= 8;
                if (get_val(x + 1, y, z))         index |= 16;
                if (get_val(x + 1, y, z + 1))     index |= 32;
                if (get_val(x, y, z + 1))         index |= 64;
                if (get_val(x, y, z))             index |= 128;

                const auto& edges = big_ass_array[index];

                const auto get_coord = [&](unsigned corner) -> vec3 {
                    switch (corner) {
                    case 0:  return corner_ + vec3(x + 1, y + 1, z + 0.5) * step_;
                    case 1:  return corner_ + vec3(x + 0.5, y + 1, z + 1) * step_;
                    case 2:  return corner_ + vec3(x, y + 1, z + 0.5) * step_;
                    case 3:  return corner_ + vec3(x + 0.5, y + 1, z) * step_;
                    case 4:  return corner_ + vec3(x + 1, y, z + 0.5) * step_;
                    case 5:  return corner_ + vec3(x + 0.5, y, z + 1) * step_;
                    case 6:  return corner_ + vec3(x, y, z + 0.5) * step_;
                    case 7:  return corner_ + vec3(x + 0.5, y, z) * step_;
                    case 8:  return corner_ + vec3(x + 1, y + 0.5, z) * step_;
                    case 9:  return corner_ + vec3(x + 1, y + 0.5, z + 1) * step_;
                    case 10: return corner_ + vec3(x, y + 0.5, z + 1) * step_;
                    case 11: return corner_ + vec3(x, y + 0.5, z) * step_;
                    default: __builtin_unreachable();
                    }
                };

                for (unsigned i = 0; i < edges.size(); i += 3) {
                    if (edges[i] == -1)
                        break;

                    vec3 v1 = get_coord(edges[i]);
                    vec3 v2 = get_coord(edges[i + 1]);
                    vec3 v3 = get_coord(edges[i + 2]);

                    m.add_triangle(v1, v2, v3);
                }
            }
        }
    }

    return m;
}

  void generate_poylgon(Mesh& m, const std::vector<vec3>& points, unsigned color_index) {
    unsigned n = points.size();
    if (n < 3)
      return;

    unsigned a = 0;
    unsigned b = n - 1;
    unsigned c = 1;
    unsigned d = n - 2;

    do {
      m.add_triangle(points[a], points[b], points[c], color_index);
      a = b;
      b = c;
      c++;

      if (c >= n)
        break;

      m.add_triangle(points[a], points[b], points[c], color_index);
      a = a;
      b = d;
      d--;

    } while (c <= d);
  }

  void generate_pseudo_cylinder(Mesh& m, vec3 pos, vec3 up,
                                float heigth, float radius_bot,
                                float radius_top, unsigned color_index,
                                vec3& normal1_bot, vec3& normal2_bot,
                                unsigned precision) {

    // Get a vector normal to up in the (up, normal1_bot) plan using the
    // Gram–Schmidt process. So we can join the previous and the new cylinder.
    vec3 normal = normal1_bot - up * (up.dot(normal1_bot) / up.len());
    normal.normalize();

    // Create and align normal2 and normal2_bot.
    vec3 normal2 = up.product(normal);
    normal2.normalize();
    if (normal2.dot(normal2_bot) < 0)
      normal2 = normal2 * -1;

    vec3 top = pos + up*heigth;

    auto draw_face = [&](vec3 top_left, vec3 top_right, vec3 bot_left, vec3 bot_right)
    {
      m.add_triangle(top_left, top_right, bot_left, color_index);
      m.add_triangle(bot_left, bot_right, top_right, color_index);
    };

    unsigned big;
    unsigned medium;
    if (precision == 2)
    {
      big = 5;
      medium = 1;
    }
    else if (precision == 1)
    {
      big = 20;
      medium = 5;
    }
    else
    {
      big = 0;
      medium = 0;
    }

    float step;
    if (radius_bot >= big)
      step = 0.1;
    else if (radius_bot >= medium)
      step = 0.2;
    else
      step = 1;

    for (float tt = 0.0; tt < 1; tt += step)
    {
      float t = tt * pi/2;
      float t2 = (tt+step) * pi/2;

      draw_face(pos + normal*radius_bot*cos(t) + normal2*radius_bot*sin(t),
          pos + normal*radius_bot*cos(t2) + normal2*radius_bot*sin(t2),
          top + normal*radius_top*cos(t) + normal2*radius_top*sin(t),
          top + normal*radius_top*cos(t2) + normal2*radius_top*sin(t2));

      draw_face(pos - normal2*radius_bot*cos(t) + normal*radius_bot*sin(t), 
          pos - normal2*radius_bot*cos(t2) + normal*radius_bot*sin(t2),
          top - normal2*radius_top*cos(t) + normal*radius_top*sin(t),
          top - normal2*radius_top*cos(t2) + normal*radius_top*sin(t2));

      draw_face(pos - normal2*radius_bot*cos(t) - normal*radius_bot*sin(t), 
          pos - normal2*radius_bot*cos(t2) - normal*radius_bot*sin(t2),
          top - normal2*radius_top*cos(t) - normal*radius_top*sin(t),
          top - normal2*radius_top*cos(t2) - normal*radius_top*sin(t2));

      draw_face(pos + normal2*radius_bot*cos(t) - normal*radius_bot*sin(t), 
          pos + normal2*radius_bot*cos(t2) - normal*radius_bot*sin(t2),
          top + normal2*radius_top*cos(t) - normal*radius_top*sin(t),
          top + normal2*radius_top*cos(t2) - normal*radius_top*sin(t2));

      draw_face(pos + normal*radius_bot*cos(t) + normal2*radius_bot*sin(t),
          pos + normal*radius_bot*cos(t2) + normal2*radius_bot*sin(t2),
          pos + normal1_bot*radius_bot*cos(t) + normal2_bot*radius_bot*sin(t),
          pos + normal1_bot*radius_bot*cos(t2) + normal2_bot*radius_bot*sin(t2));

      draw_face(pos - normal2*radius_bot*cos(t) + normal*radius_bot*sin(t), 
          pos - normal2*radius_bot*cos(t2) + normal*radius_bot*sin(t2),
          pos - normal2_bot*radius_bot*cos(t) + normal1_bot*radius_bot*sin(t),
          pos - normal2_bot*radius_bot*cos(t2) + normal1_bot*radius_bot*sin(t2));

      draw_face(pos - normal2*radius_bot*cos(t) - normal*radius_bot*sin(t), 
          pos - normal2*radius_bot*cos(t2) - normal*radius_bot*sin(t2),
          pos - normal2_bot*radius_bot*cos(t) - normal1_bot*radius_bot*sin(t),
          pos - normal2_bot*radius_bot*cos(t2) - normal1_bot*radius_bot*sin(t2));

      draw_face(pos + normal2*radius_bot*cos(t) - normal*radius_bot*sin(t), 
          pos + normal2*radius_bot*cos(t2) - normal*radius_bot*sin(t2),
          pos + normal2_bot*radius_bot*cos(t) - normal1_bot*radius_bot*sin(t),
          pos + normal2_bot*radius_bot*cos(t2) - normal1_bot*radius_bot*sin(t2));
    }

    normal1_bot = normal;
    normal2_bot = normal2;
  }
}
