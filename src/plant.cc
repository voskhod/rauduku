#include "plant.hh"
#include <iostream>
#include "matrix.hh"
#include "blob.hh"

namespace rayduku
{
  Mesh LSystem::trace(unsigned n_iter)
  {
    if (!is_parametric_ && (!length_ || !angle_))
      throw std::invalid_argument("LSystem::trace(): length and angle must be set before trace()");

    if (rules_.find(axiom_) == rules_.end())
      throw std::invalid_argument("LSystem::trace(): axiom ('"
          + std::string(1, axiom_)
          + "') is not a rule");

    Mesh m;
    std::vector<TurtleStatus> stack;
    stack.reserve(100);

    TurtleStatus cur = start_;
    cur.cylinder_n1 = cur.heading.get_row(1);
    cur.cylinder_n2 = cur.heading.get_row(2);

    bool in_polygon = false;
    std::vector<vec3> polygon;
    polygon.reserve(100);

    auto rec = [&](const std::string& commands, unsigned max_depth,
                    const std::vector<float>& args, const auto& rec_ref) -> void
    {
      for (unsigned i = 0; i < commands.size(); ++i)
      {
        char c = commands[i];

        // Check if the currenct token is a rule.
        const auto it = rules_.find(c);
        bool ignore_depth = ignore_max_depth_.count(c);
        if ((max_depth || ignore_depth) && it != rules_.end())
        {
          if (is_parametric_)
          {
            auto next_args = parse_args(commands, args, i+1, i);

            if (ignore_depth)
              rec_ref(it->second, max_depth, next_args, rec_ref);
            else
              rec_ref(it->second, max_depth - 1, next_args, rec_ref);
          }
          else // args is empty and no copy is done
          {
            if (ignore_depth)
              rec_ref(it->second, max_depth, args, rec_ref);
            else
              rec_ref(it->second, max_depth - 1, args, rec_ref);
          }
        }
        else
        {
          // Forward and draw.
          if (c == 'F')
          {
            vec3 last = cur.pos;
            vec3 dir = cur.heading.get_row(0);
            dir.normalize();

            float l = is_parametric_ ? parse_one_arg(commands, args, i+1, i) : length_;
            cur.pos = cur.pos + dir * l;

            generate_pseudo_cylinder(m, last,
                cur.heading.get_row(0),
                l,
                cur.diameter1, cur.diameter2,
                cur.color_index,
                cur.cylinder_n1, cur.cylinder_n2,
                precision_);

            // When the diameter is changed, only diameter2 is changed so that
            // the cylinder make a soft transition between the two diameter.
            // Then we update diameter1.
            cur.diameter1 = cur.diameter2;

            if (in_polygon)
              polygon.push_back(cur.pos);

            if (has_tropism_)
            {
              vec3 r = cur.heading.get_row(0).product(t_);
              float alpha = cur.flexibilty  * r.len();
              r.normalize();
              SquareMatrix4 trop = SquareMatrix4::rot(alpha, r);
              cur.heading = cur.heading * trop;
            }
          }
          // Forward but no draw.
          else if (c == 'f') {
            vec3 dir = cur.heading.get_row(0);
            dir.normalize();

            float l = is_parametric_ ? parse_one_arg(commands, args, i+1, i) : length_;
            cur.pos = cur.pos + dir * l;

            if (in_polygon)
              polygon.push_back(cur.pos);
          }

          // Rotations.
          else if (c == '+')
          {
            if (is_parametric_)
            {
              float alpha = parse_one_arg(commands, args, i+1, i);
              cur.heading = SquareMatrix4::ru(alpha) * cur.heading;
            }
            else
              cur.heading = SquareMatrix4::ru(angle_) * cur.heading;
          }
          else if (c == '-')
          {
            if (is_parametric_)
            {
              float alpha = parse_one_arg(commands, args, i+1, i);
              cur.heading = SquareMatrix4::ru(-alpha) * cur.heading;
            }
            else
              cur.heading = SquareMatrix4::ru(-angle_) * cur.heading;
          }
          else if (c == '&')
          {
            if (is_parametric_)
            {
              float alpha = parse_one_arg(commands, args, i+1, i);
              cur.heading = SquareMatrix4::rl(alpha) * cur.heading;
            }
            else
              cur.heading = SquareMatrix4::rl(angle_) * cur.heading;
          }
          else if (c == '^')
          {
            if (is_parametric_)
            {
              auto next_args = parse_args(commands, args, i+1, i);
              cur.heading = SquareMatrix4::rl(-next_args[0]) * cur.heading;
            }
            else
              cur.heading = SquareMatrix4::rl(-angle_) * cur.heading;
          }
          else if (c == '\\')
          {
            if (is_parametric_)
            {
              auto next_args = parse_args(commands, args, i+1, i);
              cur.heading = SquareMatrix4::rh(next_args[0]) * cur.heading;
            }
            else
              cur.heading = SquareMatrix4::rh(angle_) * cur.heading;
          }
          else if (c == '/')
          {
            if (is_parametric_)
            {
              float alpha = parse_one_arg(commands, args, i+1, i);
              cur.heading = SquareMatrix4::rh(-alpha) * cur.heading;
            }
            else
              cur.heading = SquareMatrix4::rh(-angle_) * cur.heading;
          }
          else if (c == '|')
            cur.heading = SquareMatrix4::ru(pi) * cur.heading;
          else if (c == '$')
          {
            vec3 vh = vec3(0, -1, 0).product(cur.heading.get_row(0));
            vec3 l = vh / vh.len();
            vec3 u = cur.heading.get_row(0).product(l);

            cur.heading.set_row(l, 1);
            cur.heading.set_row(u, 2);
          }
 
          // Change diameter.
          else if (c == '!')
          {
            if (is_parametric_)
              cur.diameter2 = parse_one_arg(commands, args, i+1, i);
            else
              cur.diameter2 /= evolve_rate_;
          }
          // Change color index.
          else if (c == '\'')
            ++cur.color_index;
          // Change flexibilty.
          else if (c == '@')
            cur.flexibilty = parse_one_arg(commands, args, i+1, i);

          // Draw polygon.
          else if (c == '{')
            in_polygon = true;
          else if (c == '}')
          {
            if (!in_polygon)
              throw std::invalid_argument("LSystem::trace(): expected '{' before '}'");
            in_polygon = false;
            generate_poylgon(m, polygon, cur.color_index);
            polygon.clear();
          }

          // Save or restore current position and heading
          else if (c == '[')
            stack.push_back(cur);
          else if (c == ']')
          {
            if (stack.size() == 0)
              throw std::invalid_argument("LSystem::trace(): expected '[' before ']'");

            cur = stack.back();
            stack.pop_back();
          }
          // Ignore all parameters of a rule wich has not been derived since
          // max_depth == 0.
          else if (c == '(')
          {
            for (; commands[i] != ')'; ++i)
              if (i == commands.size())
                throw std::invalid_argument("LSystem::trace(): expected ')' after '('");
          }

          // Checks if there is any illegal token.
          else if (rules_.find(c) == rules_.end())
          {
            std::cerr << "LSystem::trace(): unkown command '" << c << "' (ascii code: "
              << (unsigned)c << "), skip and continue\n";
            print_pos(commands, i);
          }
        }
      }
    };

    rec(rules_[axiom_], n_iter - 1, axiom_args_, rec);

    return m;
  }

  Mesh generate_tree()
  {
    return LSystem('W')
      .enable_parametric({10, 1})
      .enable_randomization_variables(0.1)
      .add_rule('W', "F(_0)A(_0,_1)")
      .add_rule('A', "!(_1)D(_0)[&(a0)B(_0*r2,_1*wr)]/(d)A(_0*r1,_1*wr)")
      .add_rule('B', "!(_1)D(_0)[-(a2)$C(_0*r2,_1*wr)L(#*fs)]C(_0*r1,_1*wr)")
      .add_rule('C', "!(_1)D(_0)[+(a2)$B(_0*r2,_1*wr)L(#*fs)]B(_0*r1,_1*wr)")
      .add_rule('D', "F(_0)")
      .add_rule('L', "['^(fa)^(fa)+(#*la)&(#*la)\\(#*la){-(fa)f(_0)+(fa)f(_0)+(fa)f(_0)-(fa)|(fa)-(fa)f(_0)+(fa)f(_0)+(fa)f(_0)}]")
      .add_variables({{"r1", 0.9},
                      {"r2", 0.7},
                      {"a0", rad(30)},
                      {"a2", -rad(30)},
                      {"d", rad(137.5)},
                      {"wr", 0.707},
                      {"fa", rad(22.5)},
                      {"fs", 1},
                      {"la", rad(30)}})
      .set_diameter(1, 2)
      .trace(10);
  }

  Mesh generate_tree11()
  {
    return LSystem('A')
      .set_diameter(1, 2)
      .enable_parametric({20, 1})
      .add_rule('A', "!(_1)F(_0)[&(a1)B(_0*r1,_1*wr)]/(180)[&(a2)B(_0*r2,_1*wr)]")
      .add_rule('B', "!(_1)F(_0)[+(a1)$B(_0*r1,_1*wr)][-(a2)$B(_0*r2,_1*wr)]")
      .add_variables({{"r1", 0.9},
                      {"r2", 0.7},
                      {"a1", rad(10)},
                      {"a2", rad(60)},
                      {"wr", 0.707},
                      {"180", rad(180)}})
      .trace(10);
  }

  Mesh generate_nenuphar()
  {
    return LSystem('W')
      .set_length(0.1)
      .set_diameter(0.01, 2)
      .enable_parametric({1,rad(90)})
      .add_rule('W', "{f(_0,_1)f(_0,_1)f(_0,_1)+(_0)f(r,0)-(pi)-(a)f(r,0)}")
      .add_rule('f', "f(_0*0.5,0)+(_1*0.5)f(_0*0.5,0)+(_1*0.5)")
      .add_variables({{"0.5", 0.5},
                      {"0", 0},
                      {"pi", pi/2},
                      {"-1", -1},
                      {"r", 1.20/2},
                      {"a", asin(0.6)}})
      .trace(8);
  }

  Mesh generate_spruce()
  {
    return LSystem('W')
      .set_diameter(1, 2)
      .enable_parametric({18,1})
      .enable_tropism(0.1, {0, -1, 0})
      .enable_randomization_variables(0.05, 1)
      .set_precision(2)
      .add_rule('W', "T(_0*sbl,_1)")
      
      .add_rule('T', "!(_1)F(tl*_0)&(pi)"
                      "[^(b)$()B(_0,_1)]+(a)"
                      "[^(b)$()B(_0,_1)]+(a)"
                      "[^(b)$()B(_0,_1)]+(a)"
                      "^(pi)/(d)"
                      "F(_0)&(pi)!(tl*_0*0.5)"
                      "[^(bb)$()B(_0,_1)]+(a)"
                      "[^(bb)$()B(_0,_1)]+(a)"
                      "[^(bb)$()B(_0,_1)]+(a)"
                      "^(pi)/(d)T(_0+rtl,_1*sbl)")

      .add_rule('B', "C(_0,_1)")
      .add_rule('C', "!(_1*rt)"
                      "F(_0)"
                      "[^(b)+(sba)C(_0*sbl,_1*sbl)]"
                      "[^(b)-(sba)C(_0*sbl,_1*sbl)]"
                      "[{^(sba)f(plen)+(20)f(phyp)+(70)f(plen)}]"
                      "[{&(sba)f(plen)+(20)f(phyp)+(70)f(plen)}]"
                      "F(_0)C(sbl*_0,_1*sbl)")
      .add_variables({{"b", rad(20)},  // angle between branchs (invert a and b)
                      {"bb", rad(20)},
                      {"a", rad(360/3)},     // angle offset between two stage of branchs
                      {"d", rad(360/6)},     // angle offset between two stage of branchs
                      {"rtl", -2},       // ratio truck / banchs len
                      {"rt", 0.8},        // ratio truck / banchs
                      {"sba", rad(40)},   // sub branhc angle
                      {"sbl", 0.5},       // sub branhc ratio len
                      {"pi", pi/2.0},
                      {"plen", 1},
                      {"tl", 1.5},
                      {"0.5", 0.5},
                      {"phyp", 1/cos(rad(10))},
                      {"20", rad(170)},
                      {"70", rad(80)},
                      })
      .trace(11);
  }

  Mesh generate_tree2()
  {
    return LSystem('W')
      .set_diameter(46.2, 0)
      .enable_parametric({1, 10, rad(45)})
      .enable_tropism(0.27, {-0.2, -1, 0})
      .enable_randomization_variables(0.1, 2)
      .add_rule('W', "F(_1*2*2*2)F(_1*2)F(_1*2)/(_2)A()")
      .add_rule('A', "R(vr)F(l)[&(a)L()F(l)A()L()]/(d1)[&(a)L()F(l)A()L()]/(d2)[&(a)L()F(l)A()L()]")
      .add_rule('F', "F(_0*lr)")
      .add_rule('R', "!(_0*vr)R(_0*vr)")
      .add_rule('L', "[@(flex)!(dt)N()N()N()]")
      .add_rule('N', "F(ft)[M()+(pi)][M()]F(ft)[M()]")
      .add_rule('M', "[^(fa)^(fa){-(fa)f(fs)+(fa)f(fs)+(fa)f(fs)-(fa)|(fa)-(fa)f(fs)+(fa)f(fs)+(fa)f(fs)}]")
      .ignore_max_depth_for('L')
      .ignore_max_depth_for('M')
      .ignore_max_depth_for('N')
      .add_variables({{"d1", rad(112.5)},
                      {"d2", rad(157.5)},
                      {"a", rad(22.5)},
                      {"lr", 1.12},
                      {"vr", 1.732},
                      {"l", 50},
                      {"ft", 25},
                      {"dt", 0.5},
                      {"fa", rad(20)},
                      {"2", 2},
                      {"pi", pi},
                      {"flex", 1},
                      {"fs", 4}})
      .trace(8);
  }

  Mesh generate_flower()
  {
    return LSystem('A')
      .set_length(0.1)
      .set_diameter(0.01, 2)
      .add_rule('A', "[&&&P/W//W//W//W//W//W//W//W//W//W]")
      .add_rule('P', "FF")
      .add_rule('W', "[^F][{&&&&-f+f|-f+f}]")
      .set_angle(rad(18))
      .trace(0);
  }

  Mesh generate_branch1()
  {
    return LSystem('X')
      .set_length(0.1)
      .set_angle(rad(25))
      .add_rule('X', "F-[[X]+X]+F[+FX]-X")
      .add_rule('F', "FF")
      .trace(5);
  }

  Mesh generate_branch2()
  {
    return LSystem('F')
      .set_length(0.01)
      .set_angle(rad(25.7))
      .set_diameter(0.001, 2)
      .add_rule('F', "F[+F]F[-F]F")
      .trace(5);
  }

  Mesh generate_branch_flower()
  {
    return LSystem('p')
      .set_length(0.1)
      .set_angle(rad(18))
      .add_rule('p', "i+[p+f]--//[--l]i[++l]-[pf]++pf")
      .add_rule('i', "Fs[//&&l][//^^l]Fs")
      .add_rule('s', "sFs")
      .add_rule('l', "['{+f-ff-f+|+f-ff-f}]")
      .add_rule('f', "[&&&e'/w////w////w////w////w]")
      .add_rule('e', "FF")
      .add_rule('w', "['^F][{&&&&-f+f|-f+f}]")
      .trace(5);
  }

  Mesh generate_bush()
  {
    return LSystem('A')
      .set_length(0.1)
      .set_angle(rad(22.5))
      .set_diameter(0.05, 2)
      .set_start({3,0,-1.6})
      .add_rule('A', "[&F!LA]///////[&F!LA]///////[&F!LA]")
      .add_rule('F', "S/////F")
      .add_rule('S', "FL")
      .add_rule('L', "['^^{-f+f+f-|-f+f+f}]")
      .trace(7);
  }

}
