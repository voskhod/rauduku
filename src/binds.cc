#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "grass.hh"
#include "mesh.hh"
#include "plant.hh"

namespace rayduku
{
std::string generate_grass_blades_obj(int count)
{
    Grass grass(vec3(0,0,0), 20, count, 10);
    Mesh m = grass.generate();
    return m.to_str();
}

std::string generate_grass_blade_obj(int lod)
{
    Mesh m;
    GrassBlade grass_b(vec3(0,0,0), 10, 1, 1, 10.0 / 20.0, vec3(1, 0, 0));
    grass_b.generate_triangles(m, lod);
    return m.to_str();
}

std::string generate_terrain(int terrain_side, int mesh_size)
{
    Terrain terrain(vec3(0,0,0), terrain_side, mesh_size);
    Mesh m = terrain.generate();
    return m.to_str();
}

std::string generate_terrain_with_grass(int terrain_side, int mesh_size, int count)
{
    Terrain terrain(vec3(0,0,0), terrain_side, mesh_size);
    Mesh m_terrain = terrain.generate();

    Grass grass(vec3(0,0,0), terrain_side, count, 1, terrain);
    Mesh m_grass = grass.generate();

    m_terrain.merge(std::move(m_grass));
    return m_terrain.to_str();
}

std::string generate_tree_2_obj()
{
    return generate_tree2().to_str();
}

std::string generate_tree_spruce_obj()
{
    return generate_spruce().to_str();
}

std::string generate_nenuphar_obj()
{
    return generate_nenuphar().to_str();
}

std::string trace_lsys(LSystem& ls, unsigned n)
{
    return ls.trace(n).to_str();
}

PYBIND11_MODULE(rayduku_py, m) {
    m.doc() = "Rayduku binding plugin for demo"; // optional module docstring

    m.def("generate_grass_blade_obj", &generate_grass_blade_obj);
    m.def("generate_grass_blades_obj", &generate_grass_blades_obj);
    m.def("generate_terrain_with_grass", &generate_terrain_with_grass);
    m.def("generate_terrain", &generate_terrain);
    m.def("generate_tree_2", &generate_tree_2_obj);
    m.def("generate_tree_spruce", &generate_tree_spruce_obj);
    m.def("generate_nenuphar", &generate_nenuphar_obj);

    pybind11::class_<vec3>(m, "vec3")
      .def(pybind11::init<float, float, float>());

    pybind11::class_<LSystem>(m, "LSystem")
      .def(pybind11::init<char>())
      .def("add_rule", &LSystem::add_rule)
      .def("ignore_max_depth_for", &LSystem::ignore_max_depth_for)
      .def("set_start", &LSystem::set_start)
      .def("set_diameter", &LSystem::set_diameter)
      .def("set_length", &LSystem::set_length)
      .def("set_angle", &LSystem::set_angle)
      .def("set_heading", &LSystem::set_heading)
      .def("enable_parametric", &LSystem::enable_parametric)
      .def("add_variable", &LSystem::add_variable)
      .def("add_variables", &LSystem::add_variables)
      .def("enable_tropism", &LSystem::enable_tropism)
      .def("enable_randomization_variables", &LSystem::enable_randomization_variables);
    m.def("trace", &trace_lsys);
}
}
