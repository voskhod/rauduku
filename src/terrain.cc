#include "terrain.hh"

namespace rayduku
{
Terrain::Terrain(vec3 center, float side_length, unsigned mesh_size, float max_elevation)
    : mesh_size_(mesh_size)
    , terrain_side_length_(side_length)
    , max_elevation_(max_elevation)
    , center_(center)
{
}

Terrain::Terrain(vec3 center, float side_length, unsigned mesh_size)
    : mesh_size_(mesh_size)
    , terrain_side_length_(side_length)
    , center_(center)
{
    max_elevation_ = terrain_side_length_ / 5;
}

Mesh Terrain::generate() const
{
    Mesh m;
    vec3 corner = center_ - vec3(1, 0, 1) * terrain_side_length_ / 2;
    float step = terrain_side_length_ / static_cast<float>(mesh_size_ - 1);

    float div = static_cast<float>(mesh_size_) / 2.0f;

    for (unsigned i = 0; i < mesh_size_ - 1; i++) {
        for (unsigned j = 0; j < mesh_size_ - 1; j++)
        {
            vec3 a = corner + vec3(i * step, noise_.get(i / div, j / div, 0) * max_elevation_, j * step);
            vec3 b = corner + vec3(i * step, noise_.get(i / div, (j + 1) / div, 0) * max_elevation_, (j + 1) * step);
            vec3 c = corner + vec3((i + 1) * step, noise_.get((i + 1) / div, j / div, 0) * max_elevation_, (j)*step);
            m.add_triangle(a, b, c);

            vec3 d = corner + vec3(i * step, noise_.get(i / div, (j + 1) / div, 0) * max_elevation_, (j + 1) * step);
            vec3 e = corner + vec3((i + 1) * step, noise_.get((i + 1) / div,  (j + 1) / div, 0) * max_elevation_, (j + 1) * step);
            vec3 f = corner + vec3((i + 1) * step, noise_.get((i + 1) / div, j / div, 0) * max_elevation_, j * step);
            m.add_triangle(d, e, f);
        }
    }

    return m;
}

float Terrain::get_precise_elevation(unsigned x, unsigned y, float scale) const
{
  float div = scale / 2;
  return noise_.get(x / div, y / div, 0) * max_elevation_;
}
}
