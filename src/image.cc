#include <cmath>
#include <cstring>
#include <fstream>
#include <iostream>

#include "image.hh"

namespace rayduku {
Image::Image(const std::string& filename)
{
    std::fstream f;
    f.open(filename, std::fstream::in);

    if (!f.is_open())
        throw std::invalid_argument("Image(): Could not open file: " + filename);

    char type[3];
    f.read(type, 3);
    if (strncmp(type, "P3", 2))
        throw std::invalid_argument("Image(): Not a ppm file, get "
            + std::string(type) + " intead of P3");

    f >> width_;
    f >> height_;
    unsigned max_val;
    f >> max_val;

    data_ = std::vector<vec3>(width_ * height_);
    float r, g, b;

    for (size_t i = 0; i < height_; ++i) {
        for (size_t j = 0; j < width_; ++j) {
            f >> r;
            f >> g;
            f >> b;

            r /= max_val;
            g /= max_val;
            b /= max_val;

            set(i, j, {r, g, b});
        }
    }

    f.close();
}

void Image::gamma_correct()
{
    for (size_t i = 0; i < height_; ++i)
        for (size_t j = 0; j < width_; ++j) {
            color p = get(i, j);
            set(i, j, { sqrtf(p.x()), sqrtf(p.y()), sqrtf(p.z()) });
        }
}

void Image::save_to_ppm(const std::string& filename) const
{
    std::ofstream f;
    f.open(filename);

    f << "P3\n";
    f << width_ << ' ' << height_ << '\n';
    f << "255\n";

    for (size_t i = 0; i < height_; ++i)
        for (size_t j = 0; j < width_; ++j) {
            auto p = get(i, j);

            f << static_cast<int>(p.x() * 255.999) << ' '
              << static_cast<int>(p.y() * 255.999) << ' '
              << static_cast<int>(p.z() * 255.999) << '\n';
        }

    f.close();
}
} // namespace rayduku
