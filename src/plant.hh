#pragma once

#include "mesh.hh"
#include "vec3.hh"
#include <locale>
#include <unordered_map>
#include <unordered_set>

namespace rayduku
{
// Lindenmayer systems for generating plants. Based on The Algorithmic Beauty
// of Plants by James S. Hanan, F. David Fracchia, Deborah Fowler, Martin J.M.
// de Boer & Lynn Merce (http://algorithmicbotany.org/papers/abop/abop.pdf).
//
// This class generate plants from an input grammar wich will be interpreted by
// a graphical turtle. See generate_* functions for example.
// 
// Turtle tokens:
//   F: go forward and trace a cylinder
//   f: go forward but does not trace a cylinder
//
//   +: positive rotation on the up axis
//   -: negative rotation on the up axis
//   &: positive rotation on the left axis
//   ^: negative rotation on the left axis
//   \: positive rotation on the heading axis
//   /: negative rotation on the heading axis
//   |: 180 deg rotation on the up axis
//   $: rotate the turtle to the vertical
//
//   !: change the current diameter
//   \: increment the color index
//
//   {: start a polygon (all the forward mouvement are recorded as a side)
//   }: complete a polygon
//   [: start a branch (save the current status of the turle)
//   ]: complete a branch (restore the status of the turle)
//
//   @: change the flexibility (only usefull if the tropism is activate)
//   #: the random variable, return a random value in [0,1[ (only if used in
//      parameter)
//
class LSystem
{
  public:
    // The axiom is the name of the rule to derived at the begining.
    LSystem(char axiom)
      : axiom_(axiom)
    {
      start_.pos = {0,0,0};

      start_.heading.set_row({0, -1, 0}, 0);
      start_.heading.set_row({1, 0, 0}, 1);
      start_.heading.set_row({0, 0, 1}, 2);

      start_.diameter1 = 0;
      start_.diameter2 = 0;
      
      start_.color_index = 0;
      start_.flexibilty = 0;
    }

    // A a rule is a pair of charater and string. When an rule is being parsed
    // if the character c is encoutered and the system is not at the maximum
    // recusion depth, the caracter is replace by the derived string.
    LSystem& add_rule(char c, const std::string& derived) {
      rules_.insert({c, derived});
      return *this;
    }

    // Always derived the c rule. This can be usefull for drawing leaves at the
    // end of branchs. You must be extra cautionious of potential stack
    // overflow and recusion (for ex: c -> Fc will cause an overflow).
    LSystem& ignore_max_depth_for(char c) {
      ignore_max_depth_.insert(c);
      return *this;
    }

    LSystem& set_start(vec3 pos) {
      start_.pos = pos;
      return *this;
    }

    // Set the initial diameter and evolve rate. This should almost always be
    // set.
    LSystem& set_diameter(float diameter, float evolve_rate) {
      start_.diameter1 = diameter;
      start_.diameter2 = diameter;
      evolve_rate_ = evolve_rate;
      return *this;
    }

    // Set the distance the turtle must go everytime there is a forward
    // instruction.
    LSystem& set_length(float length) {
      length_ = length;
      return *this;
    }

    // Set the default angle used for the rotation. Caution the angle must be
    // in radian.
    LSystem& set_angle(float angle) {
      if (std::abs(angle) > 2 * pi)
          std::cerr << "set_angle(): the value (" << angle << ") is not in rad\n";
      angle_ = angle;
      return *this;
    }

    // Set the heading. heading, left and up must be normals and orgonals.
    LSystem& set_heading(vec3 heading, vec3 left, vec3 up) {
      start_.heading.set_row(heading, 0);
      start_.heading.set_row(left, 1);
      start_.heading.set_row(up, 2);
      return *this;
    }

    // Enable parametric LSystem, caution if this is enable all the rules must
    // be parametric.
    LSystem& enable_parametric(const std::vector<float>& axiom_args) {
      axiom_args_ = std::move(axiom_args);
      is_parametric_ = true;
      return *this;
    }

    LSystem& add_variables(std::unordered_map<std::string, float> global_variables) {
      global_variables_ = std::move(global_variables);

      for (auto [key, val] : global_variables_)
      {
        if (key.size() && (key[0] == 'a' || isdigit(key[0])) && std::abs(val) > 2 * pi)
          std::cerr << "Warning: " << key << " seems to be angle but it's value (" << val  << ") is not in rad\n";
      }

      return *this;
    }

    LSystem& add_variable(std::string name, float val) {
      global_variables_.insert({name, val});

      if (name.size() && (name[0] == 'a' || isdigit(name[0])) && std::abs(val) > 2 * pi)
          std::cerr << "Warning: " << name << " seems to be angle but it's value (" << val  << ") is not in rad\n";

      return *this;
    }

    // Enable tropism. The vector t represent the direction where the plant is
    // growing and e represent how much infuence tropism for a branch. This can
    // be used to make tree bend by the wind or a flower in the shadow
    // searching for some light.
    LSystem& enable_tropism(float e, vec3 t) {
      has_tropism_ = true;
      t_ = t;
      t_.normalize();
      start_.flexibilty = e;
      return *this;
    }

    // Randomize a little all the variable evrytime a variable is read. This
    // help to break prefect symetry.
    LSystem& enable_randomization_variables(float var, unsigned seed=0) {
      randomize_variables_ = true;
      var_ = var;
      std::mt19937 tmp(seed);
      std::swap(gen_, tmp);
      return *this;
    }

    LSystem& set_precision(unsigned p) {
      precision_ = p;
      return *this;
    }

    // Build the plant.
    Mesh trace(unsigned n_iter);

  private:
    struct TurtleStatus {
      vec3 pos;
      SquareMatrix4 heading;

      unsigned color_index;
      float flexibilty;

      // The cylinder prop.
      float diameter1;
      float diameter2;
      vec3 cylinder_n1;
      vec3 cylinder_n2;
    };

    // Rules and axiom.
    char axiom_;
    std::unordered_map<char, std::string> rules_;
    std::unordered_set<char> ignore_max_depth_;

    // The start of the turtle and global parameters.
    TurtleStatus start_;
    float evolve_rate_ = 2;
    float length_ = 0;
    float angle_ = 0;

    bool is_parametric_ = false;
    // Parameter of the axiom.
    std::vector<float> axiom_args_;
    std::unordered_map<std::string, float> global_variables_;

    bool has_tropism_ = false;
    vec3 t_;

    bool randomize_variables_ = false;
    mutable std::mt19937 gen_;
    float var_;

    unsigned precision_ = 1;

    void print_pos(const std::string& s, unsigned pos) const
    {
      std::cerr << "Error at: " << s << '\n';
      for (unsigned i = 0; i < pos + 9; ++i)
        std::cerr << ' ';
      std::cerr << "^^^\n"; 
    };

    std::vector<float> parse_args(const std::string& expr,
                          const std::vector<float>& cur_args,
                          unsigned begin, unsigned& end) const
    {
      if (expr[begin] != '(')
      {
        throw std::invalid_argument("LSystem::trace(): expected args at " + expr + '\n');
        return std::vector<float>();
      }

      std::vector<float> args;
      args.reserve(10);

      std::string token;
      float acc = 1;

      char op = '*';

      for (end = begin + 1; end < expr.size(); ++end)
      {
        if (expr[end] == '*' || expr[end] == '+' || expr[end] == ',' || expr[end] == ')')
        {
          if (token.size())
          {
            if (token[0] == '_')
                if (op == '+')
                  acc += cur_args[token[1] - '0'];
                else
                  acc *= cur_args[token[1] - '0'];

            else if (token[0] == '#')
              acc = std::uniform_real_distribution<float>{0, 1}(gen_);

            else
            {
              try {
                float val = global_variables_.at(token);

                if (randomize_variables_)
                  val = std::normal_distribution<float>{val, var_}(gen_);

                if (op == '+')
                  acc += val;
                else
                  acc *= val;
              }
              catch (const std::out_of_range& e) {
                std::cerr << "Unkown variable '" << token << "', skip and continue\n";
              }
            }
          }

          op = expr[end];
          token = "";
        }
        else
          token += expr[end];

        if (expr[end] == ',')
        {
          args.push_back(acc);
          acc = 1;
        }
        else if (expr[end] == ')')
        {
          args.push_back(acc);
          break;
        }
      }

      return args;
    };

    float parse_one_arg(const std::string& expr,
                          const std::vector<float>& cur_args,
                          unsigned begin, unsigned& end) const
    {
      std::vector<float> tmp = parse_args(expr, cur_args, begin, end);
      if (tmp.size() == 0)
        throw std::invalid_argument("LSystem::parse_one_args(): expected at least one argument, get 0");
      return tmp[0];
    }
};

// Some plants generated by LSystem.
Mesh generate_branch1();
Mesh generate_branch2();
Mesh generate_flower();
Mesh generate_branch_flower();
Mesh generate_bush();
Mesh generate_tree();
Mesh generate_tree11();
Mesh generate_tree2();
Mesh generate_nenuphar();
Mesh generate_spruce();
}
