#pragma once

#include <memory>

#include "camera.hh"
#include "light.hh"
#include "object.hh"
#include "vec3.hh"

namespace rayduku {
class Light {
public:
    virtual float intensity() const = 0;
    virtual vec3 pos() const = 0;
};

typedef std::shared_ptr<Light> light_ptr;

class PointLight : public Light {
public:
    PointLight(vec3 pos, float intensity)
        : pos_(pos)
        , intensity_(intensity)
    {
    }

    virtual ~PointLight() { }

    static light_ptr make_point_light(vec3 pos, float intensity)
    {
        return std::make_shared<PointLight>(pos, intensity);
    }

    float intensity() const
    {
        return intensity_;
    }

    vec3 pos() const
    {
        return pos_;
    }

private:
    vec3 pos_;
    float intensity_;
};
} // namespace rayduku
