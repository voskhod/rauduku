#pragma once

#include <vector>

#include "mesh.hh"
#include "vec3.hh"

namespace rayduku {

// Generate a blob structure from potentials points.
class Blob {
  public:
      struct potential_point {
          potential_point(const vec3& pos, float val)
              : pos(pos)
              , val(val)
          {}

          explicit potential_point(const vec3& pos)
              : pos(pos), val(1)
          {}
          vec3 pos;
          float val{};
      };

      Blob(vec3 center, float side_size, int step_count);

      void add_potential_point(const potential_point& pp) {
          potential_points.push_back(pp);
      }

      Mesh generate(float potential);

  private:
      float find_potential(int x, int y);
      float find_potential(int x, int y, int z);

      std::vector<potential_point> potential_points;
      vec3 corner_;
      float step_;
      int step_count_;
  };

  // The points must be sorted and in the same plane. The polygon must be
  // convex.
  void generate_poylgon(Mesh& m, const std::vector<vec3>& points, unsigned color_index);

  // Generate a cylinder.
  void generate_pseudo_cylinder(Mesh& m,      // The mesh in wich the cylinder will be added
      vec3 pos,                               // The bottom center
      vec3 up,                                // The up direction, must be normalized
      float heigth,                           // The cylinder height
      float radius_bot, float radius_top,     // The top and bottom radius
      unsigned color_index,                   // The color of the cylinder
      vec3& normal1_bot, vec3& normal2_bot,   // The normal of the previous cylinder. This is
                                              // used to align the triangle of two consecutive
                                              // cylinder. At  the end of the function, there
                                              // are changed by the top normal for the drawn
                                              // cylinder.
      unsigned precision=1);                  // 0 is highest, 2 is lowesttangente
}
