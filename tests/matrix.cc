#include <cassert>

#include "matrix.hh"
#include "vec3.hh"

#include <iostream>

using rayduku::SquareMatrix4;
using rayduku::vec3;

int main()
{
  SquareMatrix4 id;
  for (unsigned i = 0; i < 4; ++i)
    for (unsigned j = 0; j < 4; ++j)
      id[i * 4 + j] = (i == j);

  const vec3 a(1, 2, 3);

  vec3 c = id * a;
  assert(c.x() == 1 && c.y() == 2 && c.z() == 3 && "id");

  for (unsigned i = 0; i < 4; ++i)
    for (unsigned j = 0; j < 4; ++j)
      id[i * 4 + j] = i * 4 + j;

  c = id * a;
  assert(c.x() == 32 && c.y() == 38 && c.z() == 44 && "mul");
}
