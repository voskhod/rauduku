#include <cassert>

#include "object.hh"
#include "vec3.hh"

using namespace rayduku;

int main()
{
  auto texture = UniformTexture::make_uniform_texture({1.0, 0.0, 0.0}, 0);
  auto material = Matt::make_matt();
  auto sphere = Sphere::make_sphere({0, 0, 5}, 1, texture, material);

  vec3 origin = {0, 0, 0};
  vec3 direction = {0, 0, 1};

  assert(sphere->intersects(direction, origin) != 0);

  direction = {0, 1, 1};
  assert(sphere->intersects(direction, origin) == 0);

  auto far_sphere = Sphere::make_sphere({0, -101, 0},
                    100,
                    texture, material);

  direction = vec3{0, 101, 0};
  assert(far_sphere->intersects(direction, origin));

  direction = vec3{0, 0, 0};
  assert(far_sphere->intersects(direction, origin) == 0);
}
