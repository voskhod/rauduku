#include <cassert>

#include "vec3.hh"

using rayduku::vec3;

int main()
{
  return 0;
  const vec3 a(1, 2, 3);
  const vec3 b(4, 5, 6);
  const vec3 aa(-1, 2, 3);

  vec3 c = a + b;
  assert(c.x() == 5 && c.y() == 7 && c.z() == 9 && "add");

  c = a - b;
  assert(c.x() == -3 && c.y() == -3 && c.z() == -3 && "sub");

  c = a * 2;
  assert(c.x() == 2 && c.y() == 4 && c.z() == 6 && "scal mul");
  
  c = aa.product(b);
  assert(c.x() == -3 && c.y() == 18 && c.z() == -13 && "product");

  assert(a.dot(b) == 32 && "dot");
  
  vec3 d{1, 2, 7};
  d.normalize();
  assert(d.x() - 0.1 < 0.1
      && d.y() - 0.2 < 0.1
      && d.z() - 0.7 < 0.1 && "normalize"); 

  assert(a.len2() - 14 < 0.1);
  assert(a.len() - 3.7 < 0.1);

  vec3 e(-1, 0.5, 2);
  e.restrict();
  assert(e.x() == 0 && e.y() == 0.5 && e.z() == 1 && "restrict");

  vec3 f = a * b;
  assert(f.x() == 4 && f.y() == 10 && f.z() == 18 && "vec mul");

  vec3 g(0, 0, 0);
  g.normalize();
  assert(g.x() == 0 && g.y() == 0 && g.z() == 0 && "normalize = 0");

  return 0;
}
