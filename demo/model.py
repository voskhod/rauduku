import numpy as np
import plotly.graph_objects as go

def obj_data_to_mesh3d(odata):
    # odata is the string read from an obj file
    vertices = []
    faces = []
    lines = odata.splitlines()   
   
    for line in lines:
        slist = line.split()
        if slist:
            if slist[0] == 'v':
                vertex = np.array(slist[1:], dtype=float)
                vertices.append(vertex)
            elif slist[0] == 'f':
                face = []
                for k in range(1, len(slist)):
                    face.append([int(s) for s in slist[k].replace('//','/').split('/')])
                if len(face) > 3: # triangulate the n-polyonal face, n>3
                    faces.extend([[face[0][0]-1, face[k][0]-1, face[k+1][0]-1] for k in range(1, len(face)-1)])
                else:    
                    faces.append([face[j][0]-1 for j in range(len(face))])
            else: pass
    
    
    return np.array(vertices), np.array(faces)  

def show_obj(obj_data, title, width=900, height=800):
    vertices, faces = obj_data_to_mesh3d(obj_data)
    from plotly.offline import download_plotlyjs, init_notebook_mode,  iplot
    init_notebook_mode(connected=True)
    x, y, z = vertices[:,:3].T
    I, J, K = faces.T

    mesh = go.Mesh3d(
                x=x,
                y=z,
                z=y,
                vertexcolor=(255, 255, 255), #the color codes must be triplets of floats  in [0,1]!!                      
                i=I,
                j=J,
                k=K,
                name='',
                showscale=False)
    mesh.update(lighting=dict(ambient= 0.18,
                          diffuse= 1,
                          fresnel=  .1,
                          specular= 1,
                          roughness= .1),
                         
            lightposition=dict(x=10000,
                               y=20000,
                               z=15000));
    layout = go.Layout(title=title,
                   font=dict(size=14, color='black'),
                   width=width,
                   height=height,
                   scene=dict(xaxis=dict(visible=True),
                              yaxis=dict(visible=True),  
                              zaxis=dict(visible=True), 
                              aspectmode="data",
                              aspectratio=dict(x=1.5,
                                               y=0.9,
                                               z=0.5
                                         ),
                              camera=dict(eye=dict(x=6., y=0.1, z=0.1)),
                        ), 
                  paper_bgcolor='rgb(235,235,235)',
    margin=dict(
        l=0,
        r=0,
        b=0,
        t=0,
        pad=4
    ))
    return (mesh,layout)
